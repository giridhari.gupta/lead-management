
import './App.css';
import { Route, Routes, useNavigate } from 'react-router-dom';
import Dashboard from './components/Dashboard';
import Login from './pages/auth/Login';
import AllLeads from './pages/allLeads/AllLeads';
import NewLeads from './pages/NewLeads/NewLeads';
import ProtectedRoute from './route/ProtectedRoute';
import ForgotPassword from './pages/auth/ForgotPassword';
import Error404 from './pages/error/Error404';
import ManageUser from './pages/manageUser/ManageUser';
import UpdateUser from './pages/manageUser/UpdateUser';
import AddUser from './pages/manageUser/AddUser';
// import {LeadProvider} from './context/LeadContext';




function App() {
  const navigate=useNavigate()
console.log(localStorage.getItem("userlogin"))
  
  return (

    <>
      <Routes>
        <Route element={<ProtectedRoute />}>
          <Route path='/dashboard' exact element={<Dashboard />} />
          <Route path='/newleads' element={<NewLeads />} />
          <Route path='/all_leads' element={<AllLeads />} />
          <Route path='/manage_user' element={<ManageUser />} />
          <Route path="/update_user" element={<UpdateUser />} />
          <Route path="/add_user" element={<AddUser />} />


        </Route>
        {/* <Route path='/dashboard'  element={<Login/>} /> */}
        {/* <Route path='/' element={localStorage.getItem("userlogin") ? <Dashboard /> : <Login />} /> */}
        <Route path='/' element={localStorage.getItem("userlogin")!==null ? <Dashboard /> : <Login />} />


        {/* <Route path='/draft' element={<Draft />} /> */}
        {/* <Route path='/alldraft' element={<Table />} /> */}
        <Route path='/forgot-password' element={<ForgotPassword />} />
        <Route path='/*' element={<Error404 />} />

      </Routes>

    </>





  );
}

export default App;
