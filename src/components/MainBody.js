import axios from "axios";
import { useEffect, useState } from "react";
import { BASE_URL } from "../util/constants";

const MainBody = () => {
  const [totalSteps, setTotalSteps] = useState({
    Bid: 0,
    Response: 0,
    Meeting_Scheduled: 0,
    Negociation: 0,
    Waiting_for_Response: 0,
    Win: 0,
    Loose: 0
  })

  useEffect(() => {
    const token = JSON.parse(localStorage.getItem("userlogin")).access.token
    if (token) {
      axios
        .get(`${BASE_URL}/v1/lead/totalsteps`, {
          headers: {
            Authorization: `Bearer ${token}`,
          }
        })
        .then((response) => {
          console.log(response.data.result)
          setTotalSteps({ ...response.data.result })



        })
        .catch((response) => {

        });
    }






  }, [])

  return (
    <>

      <div className="row">
        <div className="col grid-margin stretch-card">
          <div className="card">
            <div className="card-body">
              <form className="forms-sample row">
                <div className="form-group col-md-2">
                  Total Bids
                </div>
                <div className="form-group col-md-2">
                  {totalSteps.Bid}
                </div>

              </form>
              <form className="forms-sample row">
                <div className="form-group col-md-2">
                  Total Response
                </div>
                <div className="form-group col-md-2">
                  {totalSteps.Response}
                </div>

              </form>
              <form className="forms-sample row">
                <div className="form-group col-md-2">
                  Total Meeting_Scheduled
                </div>
                <div className="form-group col-md-2">
                  {totalSteps.Meeting_Scheduled}
                </div>

              </form>
              <form className="forms-sample row">
                <div className="form-group col-md-2">
                  Total Negociation
                </div>
                <div className="form-group col-md-2">
                  {totalSteps.Negociation}
                </div>

              </form>
              <form className="forms-sample row">
                <div className="form-group col-md-2">
                  Total Waiting_for_Response
                </div>
                <div className="form-group col-md-2">
                  {totalSteps.Waiting_for_Response}
                </div>

              </form>
              <form className="forms-sample row">
                <div className="form-group col-md-2">
                  Total Win
                </div>
                <div className="form-group col-md-2">
                  {totalSteps.Win}
                </div>

              </form>
              <form className="forms-sample row">
                <div className="form-group col-md-2">
                  Total Loose
                </div>
                <div className="form-group col-md-2">
                  {totalSteps.Loose}
                </div>

              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}
export default MainBody