
import { NavLink, useNavigate } from "react-router-dom"


const PartialSidebar = () => {
  // const navigate=useNavigate()

  // const logout = () => {
  //   localStorage.removeItem("userlogin")
  //   //  navigate('/')

  // }

  return (
    <>
      <nav className="sidebar sidebar-offcanvas" id="sidebar">
        <ul className="nav">
          <li className="nav-item nav-category">Main</li>
          <li className="nav-item">
            <NavLink className="nav-link" to='/dashboard'>
              <span className="icon-bg"><i className="mdi mdi-cube menu-icon"></i></span>
              <span className="menu-title">Dashboard</span>
            </NavLink>
          </li>
          <li className="nav-item">
            {/* <a className="nav-link" data-bs-toggle="collapse" href="#auth1" role="button" aria-expanded="false" aria-controls="auth1">
                <span className="icon-bg"><i className="mdi mdi-lock menu-icon"></i></span>
                <span className="menu-title">Drafts</span>
                <i className="menu-arrow"></i>
              </a> */}
            <div className="collapse" id="auth1">
              <ul className="nav flex-column sub-menu">
                {/* <li className="nav-item"> <NavLink className="nav-link" to='/draft'> New Draft </NavLink></li>
                  <li className="nav-item"> <NavLink className="nav-link" to='/alldraft'> All Drafts </NavLink></li> */}

              </ul>
            </div>
          </li>
          <li className="nav-item">
            <a className="nav-link" data-bs-toggle="collapse" href="#auth" role="button" aria-expanded="false" aria-controls="auth">
              <span className="icon-bg"><i className="mdi mdi-lock menu-icon"></i></span>
              <span className="menu-title">Leads</span>
              <i className="menu-arrow"></i>
            </a>
            <div className="collapse" id="auth">
              <ul className="nav flex-column sub-menu">

                <li className="nav-item"> <NavLink className="nav-link" to='/newleads'> New Leads </NavLink></li>
                <li className="nav-item"> <NavLink className="nav-link" to='/all_leads'> All Leads </NavLink></li>
              </ul>
            </div>
          </li>

          {
            JSON.parse(localStorage.getItem("role")).role === "admin" && <li className="nav-item">

              <NavLink to="/manage_user" className="nav-link">
                <span className="icon-bg"><i className="mdi mdi-account-multiple menu-icon"></i></span>
                <span className="menu-title">Manage Users</span>
              </NavLink>

            </li>
          }


          {/* <li className="nav-item sidebar-user-actions">
            <div className="sidebar-user-menu">
              <NavLink  onClick={logout} className="nav-link"><i className="mdi mdi-logout menu-icon"></i>
                <span className="menu-title">Log Out</span></NavLink>

            </div>

          </li> */}
        </ul>
      </nav>
    </>
  )
}
export default PartialSidebar