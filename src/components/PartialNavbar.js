import { NavLink,useNavigate } from "react-router-dom"

const PartialNavbar = () => {
  const navigate=useNavigate()

  const logout = () => {
    localStorage.removeItem("userlogin")
     navigate('/')

  }
  return (
    <>
      <nav className="navbar default-layout-navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
        <div className="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
          {/* <a className="navbar-brand brand-logo" href="index.html"><img src="assets/images/logo.svg" alt="logo" /></a>
          <a className="navbar-brand brand-logo-mini" href="index.html"><img src="assets/images/logo-mini.svg" alt="logo" /></a> */}
          <NavLink to='/dashboard' className="nav-link" >Lead Management</NavLink>
        </div>
        <div className="navbar-menu-wrapper d-flex align-items-stretch">
          <button className="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
            <span className="mdi mdi-menu"></span>
          </button>
       
          <ul className="navbar-nav navbar-nav-right">
        
            <li className="nav-item nav-language dropdown d-none d-md-block">
              <a className="nav-link dropdown-toggle" id="profileDropdown" href={"/#"} data-bs-toggle="dropdown" aria-expanded="false">
                {/* <div className="nav-profile-img">
                  <img src="assets/images/faces/face28.png" alt="image" />
                </div> */}
                <div className="nav-profile-text">
                  <p className="mb-1 text-black">{JSON.parse(localStorage.getItem("role")).name}</p>
                </div>
              </a>
              <div className="dropdown-menu navbar-dropdown dropdown-menu-right p-0 border-0 font-size-sm" aria-labelledby="profileDropdown" data-x-placement="bottom-end">
                {/* <div className="p-3 text-center bg-primary">
                  <img className="img-avatar img-avatar48 img-avatar-thumb" src="assets/images/faces/face28.png" alt="" />
                </div> */}
                <div className="p-2">
                
                
                  <a className="dropdown-item py-1 d-flex align-items-center justify-content-between" href={"/#"}>
                    <span>Profile</span>
                    <span className="p-0">
                     
                      <i className="mdi mdi-account-outline ms-1"></i>
                    </span>
                  </a>
                
                  <div role="separator" className="dropdown-divider"></div>
                 
                
              
                   
                <span style={{cursor:"pointer"}} onClick={logout} className="menu-title">Log Out</span>
                  
                </div>
              </div>
            </li>
         
        
          </ul>
          <button className="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
            <span className="mdi mdi-menu"></span>
          </button>
        </div>
      </nav>
    </>
  )
}
export default PartialNavbar