// import Footer from './Footer';
import MainBody from './MainBody';
import PartialNavbar from './PartialNavbar';
import PartialSidebar from './PartialSidebar';
// import TopBanner from './TopBanner';
const Dashboard = () => {
  return (
    <>
      <div className="container-scroller">
        {/* <TopBanner /> */}

        {/* <!-- partial:partials/_navbar.html --> */}
        <PartialNavbar />
        {/* <!-- partial --> */}
        <div className="container-fluid page-body-wrapper">
          {/* <!-- partial:partials/_sidebar.html --> */}
          <PartialSidebar />
          {/* <!-- partial --> */}
          <div className="main-panel">
            <MainBody />
            {/* <!-- content-wrapper ends -->
          <!-- partial:partials/_footer.html --> */}
            {/* <Footer /> */}
            {/* <!-- partial --> */}
          </div>
          {/* <!-- main-panel ends --> */}
        </div>
        {/* <!-- page-body-wrapper ends --> */}
      </div>
      {/* <!-- container-scroller -->*/}
    </>
  )
}
export default Dashboard