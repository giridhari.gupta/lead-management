import { createContext, useState } from "react";
const LeadContext = createContext();
const LeadDispatchContext = createContext();
function LeadProvider({ children }) {
    const [leadDetails, setLeadDetails] = useState({});
  
    return (
      <LeadContext.Provider value={leadDetails}>
        <LeadDispatchContext.Provider value={setLeadDetails}>
          {children}
        </LeadDispatchContext.Provider>
      </LeadContext.Provider>
    );
  }
  export { LeadProvider, LeadContext, LeadDispatchContext };