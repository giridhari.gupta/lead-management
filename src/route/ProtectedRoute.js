import { Navigate, Outlet } from "react-router-dom";

const ProtectedRoute=()=>{
  
    let auth
    let authWithout
    const auth1 = localStorage.getItem("userlogin")
    // console.log(auth1);
   if(auth1){
   auth=true

   }
   else if (!auth1){
    authWithout=true
   }
   else{
   auth=false
   }
 
   
   return(
   auth? <Outlet/>:<Navigate to='/'/>
 

   )
}
export default ProtectedRoute