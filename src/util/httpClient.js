const { fetch: originalFetch } = window;
const requestCounter = [];

window.fetch = async (...args) => {
  let [resource, config] = args;
  // request interceptor here
  await _requestInterceptor(args)
  const response = await originalFetch(resource, config);
  // response interceptor here
  await _responseInterceptor(response)
  return response;
};

const _requestInterceptor = async (args) => {
  console.log('In request interceptor...!');
}

const _responseInterceptor = async (response) => {
  console.log('In response interceptor...!');
}

/**
 * Get token from local storage
 * @returns {String} token
 */
const _getToken = () => {
  let token;
  return token;
}

/**
 * Set token to local storage
 * @param {String} token 
 */
const _setToken = (token) => {

}

/**
 * Inject authorization token to headers
 * @param {Headers} headers 
 * @param {String} token 
 * @param {Boolean} outOfToken 
 * @returns {Headers} headers
 */
const _tokenInjector = async (headers, token, outOfToken) => {
  // Inject token in header if token not null and outOfToken not true

  return headers
}

const _genarateToken = () => {
  // get refresh token
  let token;
  // save in local storage

  return token
}

const _completePreviousCall = async (resource, config, token) => {
  const headers = _tokenInjector(config.headers, token)
  // replace header
  config.headers = headers
  const response = await originalFetch(resource, config);

  return response;
}

const hasAnyRequest = requestCounter.length ? true : false

const get = ({ url, body = {}, headers = {}, outOfToken = false, isLoaderEnable = true }) => {
  fetch(url, {
    method: 'GET',
    body: JSON.stringify({ ...body, ...{ outOfToken } }),
    headers: new Headers({
      ...{
        'Content-Type': 'application/json; charset=UTF-8',
      }, ...headers
    }),
  })
    .then((response) => response.json())
    .then((json) => console.log(json));
}

const post = ({ url, body = {}, headers = {}, outOfToken = false, isLoaderEnable = true }) => {
  fetch(url, {
    method: 'POST',
    body: JSON.stringify({ ...body, ...{ outOfToken } }),
    headers: new Headers({
      ...{
        'Content-Type': 'application/json; charset=UTF-8',
      }, ...headers
    }),
  })
    .then((response) => response.json())
    .then((json) => console.log(json));
}

