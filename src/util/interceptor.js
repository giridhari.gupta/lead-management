import axios from "axios";
import { BASE_URL } from "./constants";
function getLocalAccessToken() {
  const accessToken = JSON.parse(localStorage.getItem("userlogin")).access.token;;
  return accessToken;
}

function getLocalRefreshToken() {
  const refreshToken = JSON.parse(localStorage.getItem("userlogin")).refresh.token;
  return refreshToken;
}

const instance = axios.create({
  baseURL: BASE_URL,
  headers: {
    "Content-Type": "application/json",
  },
});

instance.interceptors.request.use(
  (config) => {
    const token = getLocalAccessToken();
    if (token) {
      config.headers["x-access-token"] = token;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

instance.interceptors.response.use(
  (res) => {
    console.log(res)
    return res;
  },
  async (err) => {
    const originalConfig = err.config;
    console.log("err.response", err.response)
    console.log("originalConfig",originalConfig)


    if (err.response) {
      // Access Token was expired
      if (err.response.status === 401 && !originalConfig._retry) {
        console.log("checking condition")
        originalConfig._retry = true;
        // console.log("originalConfig", originalConfig)

        try {
          const rs = await refreshToken();
          console.log("rs", rs.data.access.token)
          const accessToken = rs.data;
          // console.log(accessToken)
          // console.log(accessToken)
          // const userlogin=JSON.parse(localStorage.getItem("userlogin"));
          // userlogin.access.token=accessToken

          // console.log(userlogin)
          localStorage.setItem("userlogin", JSON.stringify(accessToken));


          instance.defaults.headers.common["x-access-token"] = accessToken.access.token;

          return instance(originalConfig);
        } catch (_error) {
          if (_error.response && _error.response.data) {
            return Promise.reject(_error.response.data);
          }

          return Promise.reject(_error);
        }
      }

      if (err.response.status === 403 && err.response.data) {
        return Promise.reject(err.response.data);
      }
    }

    return Promise.reject(err);
  }
);



function refreshToken() {
  return instance.post("/v1/auth/refresh-tokens", {
    refreshToken: getLocalRefreshToken(),
  });
}
export { instance }