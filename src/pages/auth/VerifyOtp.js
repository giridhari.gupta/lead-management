import axios from "axios";
import { useState } from "react";
import ResetPassword from "./ResetPassword";
import { useContext } from "react";
import UserContext from "../../context/UserContext";
import { BASE_URL } from "../../util/constants";

const VerifyOtp = () => {
  const email = useContext(UserContext)
  const [verified, setverified] = useState(false);
  const [otp, setOtp] = useState({ otp: "", email: email });
  const handleKeyDown = (e) => {
    console.log(e)
    // if(!/[0-9]+$/.test(e.key)){
    //   e.preventDefault()
    // }
    if ((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105)) {
      return e.keyCode
    }
  }

  const getdata = (e) => {

    const { value, name } = e.target;


    setOtp(() => {
      return {
        ...otp,
        [name]: value,
      };
    });


  };

  const addData = async (e) => {
    e.preventDefault();

    console.log(otp, "<<== otp...")

    if (!otp) {
      alert("OTP must be filled");
    }
    else {
      axios
        .post(`${BASE_URL}/v1/auth/verify-otp`, {
          ...otp,
        })
        .then((data) => {
          alert(data.data.serverResponse.message)


          setverified(true);
        })
        .catch((error) => {

          alert(error.response.data.message);
          setOtp("")
        });
    }
  };

  return (
    <>
      {verified ? (
        <UserContext.Provider value={{ email: email, otp: otp.otp }}><ResetPassword /></UserContext.Provider>
      ) : (
        <div className="container-scroller">
          <div className="container-fluid page-body-wrapper full-page-wrapper">
            <div className="content-wrapper d-flex align-items-center auth">
              <div className="row flex-grow">
                <div className="col-lg-4 mx-auto">
                  <div className="auth-form-light text-left p-5">
                    <div className="brand-logo">
                      <img src="../src/pages/logo-dark.svg" alt="" />
                    </div>

                    <h6 className="font-weight-light">
                      Enter OTP You Received
                    </h6>

                    <div className="form-group">
                      <input
                        onKeyDown={handleKeyDown}
                        type="text"
                        name="otp"
                        onChange={getdata}

                        className="form-control form-control-lg"
                        placeholder="Enter OTP"
                      />
                    </div>

                    <div className="mt-3">
                      <button
                        onClick={addData}
                        className="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn"
                      >
                        Submit
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};
export default VerifyOtp;
