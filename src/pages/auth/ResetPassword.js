import axios from "axios";
import { useState } from "react";
import UserContext from "../../context/UserContext";
import { useContext } from "react";
import showPwdImg from "../../assets/images/show-password.svg";
import hidePwdImg from "../../assets/images/hide-password.svg";
import "./TogglePassword.css";
import { useNavigate } from "react-router-dom";
import { BASE_URL } from "../../util/constants";

const ResetPassword = () => {
  const navigate = useNavigate();
  const [isRevealPwd1, setIsRevealPwd1] = useState(false);
  const [isRevealPwd2, setIsRevealPwd2] = useState(false);
  const user = useContext(UserContext);

  const email = user.email;
  const otp = user.otp;
  const [inpval, setInpval] = useState({
    createPassword: "",
    confirmPassword: "",
    email: email,
    otp: otp,
  });

  const getdata = (e) => {
    const { value, name } = e.target;
    console.log(value);

    setInpval(() => {
      return {
        ...inpval,
        [name]: value,
      };
    });
  };

  const addData = async (e) => {
    e.preventDefault();
    const { createPassword, confirmPassword } = inpval;
    var RegEx = "^(?=.*[A-Za-z])(?=.*d)[A-Za-zd]{8,}$";
    var Valid = RegEx.test(inpval.confirmPassword);
    console.log(Valid);
    if (!createPassword) {
      alert("This must be filled");
    } else if (!confirmPassword) {
      alert("This must be filled");
    } else if (inpval.createPassword === inpval.confirmPassword) {
      if (
        Valid &&
        inpval.createPassword.length >= 8 &&
        inpval.confirmPassword.length >= 8
      ) {
        axios
          .post(`${BASE_URL}/v1/auth/reset-password`, {
            email: inpval.email,
            otp: inpval.otp,
            password: inpval.confirmPassword,
          })
          .then((data) => {
            console.log("response", data);
            navigate("/");
          })
          .catch((error) => {
            console.log(error.response.data.code);
          });
      } else {
        alert(
          "password should contain atleast 1 alphabet and atleast 1 number"
        );
      }
    } else {
      alert("create password and confirm password should be same");
    }
  };
  return (
    <>
      <div className="container-scroller">
        <div className="container-fluid page-body-wrapper full-page-wrapper">
          <div className="content-wrapper d-flex align-items-center auth">
            <div className="row flex-grow">
              <div className="col-lg-4 mx-auto">
                <div className="auth-form-light text-left p-5">
                  <div className="brand-logo">
                    <img src="../src/pages/logo-dark.svg" alt="" />
                  </div>

                  <h6 className="font-weight-light">Reset Your Password</h6>
                  <form className="pt-3">
                    <div className="form-group">
                      <div className="pwd-container">
                        <input
                          type={isRevealPwd1 ? "text" : "password"}
                          name="createPassword"
                          onChange={getdata}
                          className="form-control form-control-lg"
                          id="exampleInputEmail1"
                          placeholder="Create Password"
                        />
                        <img
                          title={
                            isRevealPwd1 ? "Hide password" : "Show password"
                          }
                          src={isRevealPwd1 ? hidePwdImg : showPwdImg}
                          alt={isRevealPwd1 ? "Hide" : "Show"}
                          onClick={() =>
                            setIsRevealPwd1((prevState) => !prevState)
                          }
                        />
                      </div>
                    </div>
                    <div className="form-group">
                      <div className="pwd-container">
                        <input
                          type={isRevealPwd2 ? "text" : "password"}
                          name="confirmPassword"
                          onChange={getdata}
                          className="form-control form-control-lg"
                          id="exampleInputPassword1"
                          placeholder="Confirm Password"
                        />
                        <img
                          title={
                            isRevealPwd2 ? "Hide password" : "Show password"
                          }
                          src={isRevealPwd2 ? hidePwdImg : showPwdImg}
                          alt={isRevealPwd2 ? "Hide" : "Show"}
                          onClick={() =>
                            setIsRevealPwd2((prevState) => !prevState)
                          }
                        />
                      </div>
                      <p><b style={{ color: "black" }}>Password should be of minimum 1 alphabet and 1 number</b></p>
                    </div>
                    <div className="mt-3">
                      <button
                        onClick={addData}
                        className="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn"
                      >
                        Save Password
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default ResetPassword;
