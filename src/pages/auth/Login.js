import { NavLink } from "react-router-dom";
import axios from "axios";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import showPwdImg from "../../assets/images/show-password.svg";
import hidePwdImg from "../../assets/images/hide-password.svg";
import "./TogglePassword.css";
import { BASE_URL } from '../../util/constants'

const Login = () => {
	const navigate = useNavigate();
	const [isRevealPwd, setIsRevealPwd] = useState(false);
	const [inpval, setInpval] = useState({
		email: "",
		password: "",
	});
	// console.log(BASE_URL)
	const getdata = (e) => {
		const { value, name } = e.target;
		// console.log(value)

		setInpval(() => {
			return {
				...inpval,
				[name]: value,
			};
		});
	};

	const addData = async (e) => {
		e.preventDefault();

		const { email, password } = inpval;

		if (!email && !password) {
			alert("email and password is required");
		} else if (!email && password) {
			alert(" email is required");
		} else if (!email.includes("@")) {
			alert("email id must have @");
		} else if (email && !password) {
			alert("password is required");
		} else {
			axios
				.post(`${BASE_URL}/v1/auth/login`, {
					...inpval,
				})
				.then((data) => {
					console.log(data)
					const userlogin = data.data.result.tokens;
					localStorage.setItem("userlogin", JSON.stringify(userlogin));
					localStorage.setItem("role", JSON.stringify(data.data.result.user));


					navigate("/dashboard");
				})
				.catch((error) => {
					console.log(error)
					alert(error.response.data.message);
				});
		}
	};

	return (
		<>
			<div className="container-scroller">
				<div className="container-fluid page-body-wrapper full-page-wrapper">
					<div className="content-wrapper d-flex align-items-center auth">
						<div className="row flex-grow">
							<div className="col-lg-4 mx-auto">
								<div className="auth-form-light text-left p-5">
									<div className="brand-logo">
										<img src="../src/pages/logo-dark.svg" alt="" />
									</div>
									<h4>Hello! let's get started</h4>
									<h6 className="font-weight-light">Sign in to continue.</h6>
									<form className="pt-3">
										<div className="form-group">
											<input
												type="email"
												name="email"
												onChange={getdata}
												className="form-control form-control-lg"
												id="exampleInputEmail1"
												placeholder="Enter Email"
											/>
										</div>
										<div className="form-group">
											<input
												type={isRevealPwd ? "text" : "password"}
												name="password"
												onChange={getdata}
												className="form-control form-control-lg"
												id="exampleInputPassword1"
												placeholder="Password"
											/>
											<div className="input-group-append">
												<img
													title={
														isRevealPwd ? "Hide password" : "Show password"
													}
													src={isRevealPwd ? hidePwdImg : showPwdImg}
													alt={isRevealPwd ? "Hide" : "Show"}
													onClick={() =>
														setIsRevealPwd((prevState) => !prevState)
													}
												/>
											</div>
										</div>
										<div className="mt-3">
											<button
												onClick={addData}
												className="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn"
											>
												SIGN IN
											</button>
										</div>
										<div className="my-2 d-flex justify-content-between align-items-center">
											<div className="form-check">
												<label className="form-check-label text-muted">
													<input type="checkbox" className="form-check-input" />{" "}
													Keep me signed in{" "}
												</label>
											</div>
											<NavLink
												to="/forgot-password"
												className="auth-link text-black"
											>
												Forgot password?
											</NavLink>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</>
	);
};
export default Login;
