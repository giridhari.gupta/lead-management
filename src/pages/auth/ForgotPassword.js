import axios from "axios";
import { useState } from "react";
import { NavLink } from "react-router-dom";
import UserContext from "../../context/UserContext";
import { BASE_URL } from "../../util/constants";
import VerifyOtp from "./VerifyOtp";

const ForgotPassword = () => {
  const [token, setToken] = useState(false);
  const [inpval, setInpval] = useState("");

  const getdata = (e) => {
    const { value, name } = e.target;
    console.log(value);

    setInpval({
      [name]: value,
    });
  };
  console.log(inpval);

  const addData = async (e) => {
    e.preventDefault();
    const email = inpval.email;

    if (!email) {
      alert("email is required");
    } else if (!email.includes("@")) {
      alert("email id must have @");
    } else {
      axios
        .post(`${BASE_URL}/v1/auth/forgot-password`, {
          ...inpval,
        })
        .then((data) => {
          console.log("response", data);
          setToken(true);
        })
        .catch((error) => {
          alert(error.response.data.message);
        });
    }
  };
  return (
    <>
      {token ? (
        <UserContext.Provider value={inpval.email}>
          <VerifyOtp />
        </UserContext.Provider>
      ) : (
        <div className="container-scroller">
          <div className="container-fluid page-body-wrapper full-page-wrapper">
            <div className="content-wrapper d-flex align-items-center auth">
              <div className="row flex-grow">
                <div className="col-lg-4 mx-auto">
                  <div className="auth-form-light text-left p-5">
                    <div className="brand-logo">
                      <img src="../src/pages/logo-dark.svg" alt="" />
                    </div>

                    <h6 className="font-weight-light">Enter Your Mail Id</h6>

                    <div className="form-group">
                      <input
                        type="email"
                        name="email"
                        onChange={getdata}
                        className="form-control form-control-lg"
                        id="exampleInputEmail1"
                        placeholder="Enter Email"
                      />
                    </div>

                    <div className="mt-3">
                      <button
                        onClick={addData}
                        className="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn"
                      >
                        Submit
                      </button>
                    </div>
                    <div className="my-2 d-flex justify-content-between align-items-center">
                      <div className="form-check"></div>
                      <NavLink to="/" className="auth-link text-black">
                        Back
                      </NavLink>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};
export default ForgotPassword;
