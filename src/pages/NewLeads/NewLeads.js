import PartialNavbar from "../../components/PartialNavbar"
import PartialSidebar from "../../components/PartialSidebar"
// import TopBanner from "../components/TopBanner"
import NewForm from "./NewForm"

const NewLeads = () => {
  return (
    <>
      <div className="container-scroller">
        {/* <TopBanner /> */}

        {/* <!-- partial:partials/_navbar.html --> */}
        <PartialNavbar />
        {/* <!-- partial --> */}
        <div className="container-fluid page-body-wrapper">
          {/* <!-- partial:partials/_sidebar.html --> */}
          <PartialSidebar />
          {/* <!-- partial --> */}
          <div className="main-panel">

            <NewForm />
            {/* <!-- content-wrapper ends -->
          <!-- partial:partials/_footer.html --> */}

            {/* <!-- partial --> */}
          </div>
          {/* <!-- main-panel ends --> */}
        </div>
        {/* <!-- page-body-wrapper ends --> */}
      </div>
      {/* <!-- container-scroller -->*/}
    </>
  )
}
export default NewLeads