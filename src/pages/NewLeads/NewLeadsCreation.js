import axios from "axios";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { BASE_URL } from "../../util/constants";
import { instance } from "../../util/interceptor";

const FormValidation = () => {
  const navigate = useNavigate()
  const [hasDesign, setHasDesign] = useState(false);
  const [hasWebsite, setHasWebsite] = useState(false);
  const [hasWebApp, setHasWebApp] = useState(false);
  const [hasMobileApp, setHasMobileApp] = useState(false);
  const [hasApiTechnology, setHasAPI] = useState(false);
  const [budget, setBudget] = useState(false);
  const [hasTimeline, setHasTimeline] = useState(false);
  const [hasReact, setHasReact] = useState(false);
  const [hasAngular, setHasAngular] = useState(false);
  const [hasVue, setHasVue] = useState(false);
  const [hasWordPress, setHasWordPress] = useState(false);
  const [hasNativeAndroid, setHasNativeAndroid] = useState(false);
  const [hasNativeIos, setHasNativeIos] = useState(false);
  const [hasFlutter, setHasFlutter] = useState(false);
  const [hasAPIWordPress, setHasAPIWordPress] = useState(false);
  const [hasAPINodejs, setHasAPINodejs] = useState(false);
  const [hasLaravel, setHasLaravel] = useState(false);

  const [inpval, setInpval] = useState({
    client_name: "",
    // client_email: "",
    // client_phone: "",
    // client_skype: "",
    // client_location: "",
    project_name: "",
    // project_location: "",
    // project_description: "",
    // hasDesign: hasDesign,
    // designData: "",
    // hasWebsite: hasWebsite,
    // websiteData: "",
    // hasWebApp: hasWebApp,
    // hasMobileApp: hasMobileApp,
    // hasApiTechnology: hasApiTechnology,
    // budget: budget,
    // minBudgetData: "",
    // maxBudgetData: "",
    // hasTimeline: hasTimeline,
    // minTimelineData: "",
    // maxTimelineData: "",
    web_technology: [],
    mobile_technology: [],
    API_technology: [],
    steps:"Bid"
  });
  const [validation, setValidation] = useState({
    client_name: "",
    client_email: "",
    client_phone: "",
    client_skype: "",
    client_location: "",
    project_name: "",
    project_location: "",
    project_description: "",
    hasDesign: hasDesign,
    designData: "",
    hasWebsite: hasWebsite,
    websiteData: "",
    hasWebApp: hasWebApp,
    hasMobileApp: hasMobileApp,
    hasApiTechnology: hasApiTechnology,
    budget: budget,
    minBudgetData: "",
    maxBudgetData: "",
    hasTimeline: hasTimeline,
    minTimelineData: "",
    maxTimelineData: "",
    web_technology: [],
    mobile_technology: [],
    API_technology: [],
  });
  const handleCheckBoxDesign = (e) => {
    if (e.target.checked) {
      setHasDesign((prevState) => !prevState);

      setInpval((previousState) => {
        return { ...previousState, hasDesign: !hasDesign };
      });
    } else {
      setHasDesign((prevState) => !prevState);
      setInpval((previousState) => {
        return { ...previousState, hasDesign: !hasDesign, designData: "" };
      });

      setValidation((previousState) => {
        return { ...previousState, designData: "" };
      });
    }
  };

  const handleCheckBoxWebsite = (e) => {
    if (e.target.checked) {
      setHasWebsite((prevState) => !prevState);

      setInpval((previousState) => {
        return { ...previousState, hasWebsite: !hasWebsite };
      });
    } else {
      setHasWebsite((prevState) => !prevState);
      setInpval((previousState) => {
        return { ...previousState, hasWebsite: !hasWebsite, websiteData: "" };
      });

      setValidation((previousState) => {
        return { ...previousState, websiteData: "" };
      });
    }
  };
  const handleCheckBoxWebApp = (e) => {
    if (e.target.checked) {
      setHasWebApp((prevState) => !prevState);
      setHasReact((prevState) => prevState);
      setHasAngular((prevState) => prevState);
      setHasVue((prevState) => prevState);
      setHasWordPress((prevState) => prevState);
      setInpval((previousState) => {
        return { ...previousState, hasWebApp: !hasWebApp };
      });
    } else {
      setHasReact((prevState) => false);
      setHasAngular((prevState) => false);
      setHasVue((prevState) => false);
      setHasWordPress((prevState) => false);
      setHasWebApp((prevState) => !prevState);
    }
  };
  const handleCheckBoxMobileApp = (e) => {
    if (e.target.checked) {
      setHasMobileApp((prevState) => !prevState);
      setHasNativeAndroid((prevState) => prevState);
      setHasNativeIos((prevState) => prevState);
      setHasFlutter((prevState) => prevState);

      setInpval((previousState) => {
        return { ...previousState, hasMobileApp: !hasMobileApp };
      });
    } else {
      setHasNativeAndroid((prevState) => false);
      setHasNativeIos((prevState) => false);
      setHasFlutter((prevState) => false);
      setHasMobileApp((prevState) => !prevState);
    }
  };
  const handleCheckBoxAPI = (e) => {
    if (e.target.checked) {
      setHasAPI((prevState) => !prevState);
      setHasAPIWordPress((prevState) => prevState);
      setHasAPINodejs((prevState) => prevState);
      setHasLaravel((prevState) => prevState);

      setInpval((previousState) => {
        return { ...previousState, hasApiTechnology: !hasApiTechnology };
      });
    } else {
      setHasAPI((prevState) => false);
      setHasAPIWordPress((prevState) => false);
      setHasAPINodejs((prevState) => false);
      setHasLaravel((prevState) => false);
    }
  };
  const handleCheckBoxBudget = (e) => {
    if (e.target.checked) {
      setBudget((prevState) => !prevState);
      setInpval((previousState) => {
        return { ...previousState, budget: !budget };
      });
    } else {
      setBudget((prevState) => false);
      setInpval((previousState) => {
        return { ...previousState, maxBudgetData: "", minBudgetData: "" };
      });
    }
  };
  const handleCheckBoxTimeline = (e) => {
    if (e.target.checked) {
      setHasTimeline((prevState) => !prevState);
      setInpval((previousState) => {
        return { ...previousState, hasTimeline: !hasTimeline };
      });
    } else {
      setHasTimeline((prevState) => false);
      setInpval((previousState) => {
        return { ...previousState, maxTimelineData: "", minTimelineData: "" };
      });
    }
  };

  const handleCheckReact = (event) => {
    if (event.target.checked) {
      setHasReact((prevState) => !prevState);

      setInpval((previousState) => {
        return {
          ...previousState,
          web_technology: [...previousState.web_technology, "React"],
        };
      });
    } else {
      setHasReact((prevState) => !prevState);
      setInpval((previousState) => {
        return {
          ...previousState,
          web_technology: previousState.web_technology.filter(
            (x) => x === !"React"
          ),
        };
      });
    }
  };
  const handleCheckAngular = (event) => {
    if (event.target.checked) {
      setHasAngular((prevState) => !prevState);

      setInpval((previousState) => {
        return {
          ...previousState,
          web_technology: [...previousState.web_technology, "Angular"],
        };
      });
    } else {
      setHasAngular((prevState) => !prevState);

      setInpval((previousState) => {
        return {
          ...previousState,
          web_technology: previousState.web_technology.filter(
            (x) => x === !"Angular"
          ),
        };
      });
    }
  };
  const handleCheckVue = (event) => {
    if (event.target.checked) {
      setHasVue((prevState) => !prevState);

      setInpval((previousState) => {
        return {
          ...previousState,
          web_technology: [...previousState.web_technology, "Vue"],
        };
      });
    } else {
      setHasVue((prevState) => !prevState);
      setInpval((previousState) => {
        return {
          ...previousState,
          web_technology: previousState.web_technology.filter(
            (x) => x === !"Vue"
          ),
        };
      });
    }
  };
  const handleCheckWordPress = (event) => {
    if (event.target.checked) {
      setHasWordPress((prevState) => !prevState);

      setInpval((previousState) => {
        return {
          ...previousState,
          web_technology: [...previousState.web_technology, "WordPress"],
        };
      });
    } else {
      setHasWordPress((prevState) => !prevState);

      setInpval((previousState) => {
        return {
          ...previousState,
          web_technology: previousState.web_technology.filter(
            (x) => x === !"WordPress"
          ),
        };
      });
    }
  };
  const handleCheckNativeAndroid = (event) => {
    if (event.target.checked) {
      setHasNativeAndroid((prevState) => !prevState);

      setInpval((previousState) => {
        return {
          ...previousState,
          mobile_technology: [
            ...previousState.mobile_technology,
            "NativeAndroid",
          ],
        };
      });
    } else {
      setInpval((previousState) => {
        return {
          ...previousState,
          mobile_technology: previousState.mobile_technology.filter(
            (x) => x === !"NativeAndroid"
          ),
        };
      });
    }
  };
  const handleCheckNativeIos = (event) => {
    if (event.target.checked) {
      setHasNativeIos((prevState) => !prevState);

      setInpval((previousState) => {
        return {
          ...previousState,
          mobile_technology: [...previousState.mobile_technology, "NativeIOS"],
        };
      });
    } else {
      setInpval((previousState) => {
        return {
          ...previousState,
          mobile_technology: previousState.mobile_technology.filter(
            (x) => x === !"NativeIOS"
          ),
        };
      });
    }
  };
  const handleCheckFlutter = (event) => {
    if (event.target.checked) {
      setHasFlutter((prevState) => !prevState);

      setInpval((previousState) => {
        return {
          ...previousState,
          mobile_technology: [...previousState.mobile_technology, "Flutter"],
        };
      });
    } else {
      setInpval((previousState) => {
        return {
          ...previousState,
          mobile_technology: previousState.mobile_technology.filter(
            (x) => x === !"Flutter"
          ),
        };
      });
    }
  };
  const handleCheckAPIWordPress = (event) => {
    if (event.target.checked) {
      setHasAPIWordPress((prevState) => !prevState);

      setInpval((previousState) => {
        return {
          ...previousState,
          API_technology: [...previousState.API_technology, "WordPress"],
        };
      });
    } else {
      setInpval((previousState) => {
        return {
          ...previousState,
          API_technology: previousState.API_technology.filter(
            (x) => x === !"WordPress"
          ),
        };
      });
    }
  };
  const handleCheckAPINodejs = (event) => {
    if (event.target.checked) {
      setHasAPINodejs((prevState) => !prevState);

      setInpval((previousState) => {
        return {
          ...previousState,
          API_technology: [...previousState.API_technology, "Node js"],
        };
      });
    } else {
      setInpval((previousState) => {
        return {
          ...previousState,
          API_technology: previousState.API_technology.filter(
            (x) => x === !"Node js"
          ),
        };
      });
    }
  };
  const handleCheckLaravel = (event) => {
    if (event.target.checked) {
      setHasLaravel((prevState) => !prevState);

      setInpval((previousState) => {
        return {
          ...previousState,
          API_technology: [...previousState.API_technology, "Laravel"],
        };
      });
    } else {
      setInpval((previousState) => {
        return {
          ...previousState,
          API_technology: previousState.API_technology.filter(
            (x) => x === !"Laravel"
          ),
        };
      });
    }
  };

  const getdata = (e) => {
    const { value, name } = e.target;

    setInpval((inpval) => {
      return {
        ...inpval,
        [name]: value,
      };
    });

    if (e.target.name === "client_phone") {
      if (e.target.value.length < 4) {
        setValidation((prevState) => {
          return {
            ...prevState,
            client_phone: "minimum 4 digits required",
          };
        });
      } else if (e.target.value >= 4 && e.target.value.length <= 12) {
        setValidation((prevState) => {
          return {
            ...prevState,
            client_phone: "",
          };
        });
      } else if (e.target.value.length > 12) {
        setValidation((prevState) => {
          return {
            ...prevState,
            client_phone: "maximum 12 digits allowed",
          };
        });
      }
    }
  };
  const getFileDesign = (e) => {
    // const url = `${BASE_URL}/v1/item/upload`;
    const url = `/v1/item/upload`;

    // const token = JSON.parse(localStorage.getItem("userlogin")).access.token;
    instance
      .post(
        url,
        {
          item: e.target.files[0]
        },
        {
          headers: {
            "Authorization": `Bearer ${JSON.parse(localStorage.getItem("userlogin")).access.token}`,
            "Content-Type": "multipart/form-data",

          },
        }
      )
      .then((response) => {
        setInpval((inpval) => {
          return {
            ...inpval,
            designData: response.data.result.itemPath[0],
          };
        });

      })
      .catch((response) => {
        console.log(response);
      });
  }
  const getFileMoobileApp = (e) => {
    // const url = `${BASE_URL}/v1/item/upload`;
    const url = `/v1/item/upload`;

    // const token = JSON.parse(localStorage.getItem("userlogin")).access.token;
    instance
      .post(
        url,
        {
          item: e.target.files[0]
        },
        {
          headers: {
            "Authorization": `Bearer ${JSON.parse(localStorage.getItem("userlogin")).access.token}`,
            "Content-Type": "multipart/form-data",

          },
        }
      )
      .then((response) => {
        setInpval((inpval) => {
          return {
            ...inpval,
            websiteData: response.data.result.itemPath[0],
          };
        });

      })
      .catch((response) => {
        console.log(response);
      });
  }

  const addData = async (e) => {
    e.preventDefault();
    const {
      client_name,
      client_email,

      project_name,

      hasDesign,
      hasWebsite,
      designData,
      websiteData,

      minBudgetData,
      maxBudgetData,

      minTimelineData,
      maxTimelineData,
    } = inpval;

    if (!client_name) {
      setValidation((prevState) => {
        return {
          ...prevState,
          client_name: " client name is required",
        };
      });
    }


    if (client_email && !client_email.includes("@")) {
      setValidation((prevState) => {
        return {
          ...prevState,
          client_email: "client email must have @ ",
        };
      });
    }
    if (!project_name) {
      setValidation((prevState) => {
        return {
          ...prevState,
          project_name: "project name is required",
        };
      });
    }


    if (hasDesign === true && designData === "") {
      setValidation((prevState) => {
        return {
          ...prevState,
          designData: "design must be fill",
        };
      });
    }
    if (hasDesign === true && designData !== "") {
      setValidation((prevState) => {
        return {
          ...prevState,
          designData: "",
        };
      });
    }
    if (hasWebsite === true && websiteData === "") {
      setValidation((prevState) => {
        return {
          ...prevState,
          websiteData: "website must be fill",
        };
      });
    }
    if (hasWebsite === true && websiteData !== "") {
      setValidation((prevState) => {
        return {
          ...prevState,
          websiteData: "",
        };
      });
    }

    if (budget === true && maxBudgetData < minBudgetData) {
      setValidation((prevState) => {
        return {
          ...prevState,
          maxBudgetData:
            "(maximum budget sholuld be greater than or equal to  minimum budget)",
        };
      });
    }
    if (budget === true && maxBudgetData > minBudgetData) {
      setValidation((prevState) => {
        return {
          ...prevState,
          maxBudgetData: "",
        };
      });
    }

    if (hasTimeline === true && maxTimelineData < minTimelineData) {
      setValidation((prevState) => {
        return {
          ...prevState,
          maxTimelineData:
            "(maximum time sholuld be greater than or equal to minimum time)",
        };
      });
    }
    if (hasTimeline === true && maxTimelineData > minTimelineData) {
      setValidation((prevState) => {
        return {
          ...prevState,
          maxTimelineData: "",
        };
      });
    }
    else {
      // const token = JSON.parse(localStorage.getItem("userlogin")).access.token;
      // const headers = {
      //   Authorization: `Bearer ${token}`,
      // };
      instance
        .post(
          `/v1/lead`,

          {
            ...inpval,
          },
          { headers:  {
            Authorization: `Bearer ${JSON.parse(localStorage.getItem("userlogin")).access.token}`,
          } }
        )
        .then((data) => {
          
          navigate('/all_leads'
          // , {
          //   state: { ...data.data.result.lead }
          // }
          )

          console.log(data.data.result.lead);
        })
        .catch((error) => {
          console.log(error);
        });
    }
  };

  const reset = (e) => {
    e.preventDefault()
    setInpval(() => {
      return {
        ...inpval,
        client_name: "",
        client_email: "",
        client_phone: "",
        client_skype: "",
        client_location: "",
        project_name: "",
        project_location: "",
        project_description: "",
        hasDesign: hasDesign,
        designData: "",
        hasWebsite: hasWebsite,
        websiteData: "",
        hasWebApp: hasWebApp,
        hasMobileApp: hasMobileApp,
        hasApiTechnology: hasApiTechnology,
        budget: budget,
        minBudgetData: "",
        maxBudgetData: "",
        hasTimeline: hasTimeline,
        minTimelineData: "",
        maxTimelineData: "",
        web_technology: [],
        mobile_technology: [],
        API_technology: [],
      }
    })
  }
  console.log(inpval)
  return (
    <>
       <div className="d-flex justify-content-end">
        <button onClick={addData} className="btn btn-primary me-2" > Save </button>
        </div>
      <div className="row">
        <div className="col-12 grid-margin stretch-card">
          <div className="card">
            <div className="card-body">
         
              <form className="forms-sample">
                <div className="form-group">
                  <label htmlFor="exampleInputName1">Client Name *</label>
                  <input
                    type="text"
                    className="form-control"
                    name="client_name"
                    placeholder="Client Name"
                    onChange={getdata}
                  />
                  <p style={{ color: "red", marginBottom: "10px" }}>
                    {validation.client_name}
                  </p>
                </div>

                <div className="form-group">
                  <label htmlFor="exampleInputEmail3">Client Email </label>
                  <input
                    type="email"
                    className="form-control"
                    name="client_email"
                    placeholder="Client Email"
                    onChange={getdata}
                  />
                  <p style={{ color: "red", marginBottom: "10px" }}>
                    {validation.client_email}
                  </p>
                </div>
                <div className="form-group">
                  <label htmlFor="exampleInputEmail3">Client Phone </label>
                  <input
                    value={inpval.client_phone}
                    className="form-control"
                    name="client_phone"
                    placeholder="Client Phone"
                    onChange={getdata}
                    onKeyPress={(e) =>
                      !/[0-9]/.test(e.key) && e.preventDefault()
                    }
                  />
                  <p style={{ color: "red", marginBottom: "10px" }}>
                    {validation.client_phone}
                  </p>
                </div>
                <div className="form-group">
                  <label htmlFor="exampleInputEmail3">Client Skype </label>
                  <input
                    type="text"
                    className="form-control"
                    name="client_skype"
                    placeholder="Client Skype"
                    onChange={getdata}
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="exampleInputPassword4">Client Location</label>
                  <input
                    type="text"
                    className="form-control"
                    name="client_location"
                    placeholder="Client Location"
                    onChange={getdata}
                  />
                  <p style={{ color: "red", marginBottom: "10px" }}>
                    {validation.client_location}
                  </p>
                </div>
                <div className="form-group">
                  <label htmlFor="exampleInputEmail3">Project Name *</label>
                  <input
                    type="text"
                    className="form-control"
                    name="project_name"
                    placeholder="Project Name"
                    onChange={getdata}
                  />
                  <p style={{ color: "red", marginBottom: "10px" }}>
                    {validation.project_name}
                  </p>
                </div>

                <div className="form-group">
                  <label htmlFor="exampleInputPassword4">Project Location</label>
                  <input
                    type="text"
                    className="form-control"
                    name="project_location"
                    placeholder="Project Location"
                    onChange={getdata}
                  />
                  <p style={{ color: "red", marginBottom: "10px" }}>
                    {validation.project_location}
                  </p>
                </div>
                <div className="form-group">
                  <label htmlFor="exampleInputPassword4">Project Description</label>
                  <input
                    type="text"
                    className="form-control"
                    name="project_description"
                    placeholder="Project Description"
                    onChange={getdata}
                  />
                  <p style={{ color: "red", marginBottom: "10px" }}>
                    {validation.project_description}
                  </p>
                </div>
                <div className="form-group">
                  <div className="form-check">
                    <p className="form-check-label" style={{ color: "black" }}>
                      <input
                        type="checkbox"
                        className="form-check-input"
                        onChange={handleCheckBoxDesign}
                        name="hasDesign"
                      />
                      Design Provided?

                      <i className="input-helper"></i>
                    </p>
                    <div className="form-group col-md-2" >
                      <input onChange={getFileDesign} disabled={!hasDesign} type="file"></input>
                    </div>


                    <div className="form-group">
                      <textarea
                        type="text"
                        className="form-control"
                        id="exampleInputPassword4"
                        disabled={!hasDesign}
                        placeholder="Design"
                        name="designData"
                        value={inpval.designData}
                        onChange={getdata}
                      />


                      <p style={{ color: "red", marginBottom: "10px" }}>
                        {validation.designData}
                      </p>
                    </div>
                  </div>
                </div>
                <div className="form-group">
                  <div className="form-check">
                    <p className="form-check-label" style={{ color: "black" }}>
                      <input
                        type="checkbox"
                        className="form-check-input"
                        name="hasWebsite"
                        onChange={handleCheckBoxWebsite}
                      />
                      Website/Mobile App Refrences?
                      <i className="input-helper"></i>
                    </p>
                    <div className="form-group col-md-2" >
                      <input onChange={getFileMoobileApp} disabled={!hasWebsite} type="file"></input>
                    </div>

                    <div className="form-group">
                      <input
                        type="text"
                        className="form-control"
                        id="exampleInputPassword4"
                        disabled={!hasWebsite}
                        placeholder="Website"
                        onChange={getdata}
                        value={inpval.websiteData}
                        name="websiteData"
                      />
                      <p style={{ color: "red", marginBottom: "10px" }}>
                        {validation.websiteData}
                      </p>
                    </div>
                  </div>
                </div>

                <label style={{ color: "black" }}>Technology</label>
                <div className="form-check form-check-flat form-check-primary">
                  <p className="form-check-label" style={{ color: "black" }}>
                    <input
                      type="checkbox"
                      className="form-check-input"
                      onChange={handleCheckBoxWebApp}
                    />{" "}
                    Web App
                    <i className="input-helper"></i>
                  </p>
                  <div className="form-check form-check-flat form-check-primary">
                    <p className="form-check-label" style={{ color: "black" }}>
                      <ul style={{ display: "flex", gap: "30px" }}>
                        <li className="form-check form-check-flat form-check-primary">
                          <label
                            className="form-check-label"
                            style={{ color: "black" }}
                          >
                            <input
                              type="checkbox"
                              className="form-check-input"
                              disabled={!hasWebApp}
                              onChange={handleCheckReact}
                              checked={hasReact}
                            />
                            React
                            <i className="input-helper"></i>
                          </label>
                        </li>
                        <li className="form-check form-check-flat form-check-primary">
                          <label
                            className="form-check-label"
                            style={{ color: "black" }}
                          >
                            <input
                              type="checkbox"
                              className="form-check-input"
                              disabled={!hasWebApp}
                              onChange={handleCheckAngular}
                              checked={hasAngular}
                            />
                            Angular
                            <i className="input-helper"></i>
                          </label>
                        </li>
                        <li className="form-check form-check-flat form-check-primary">
                          <label
                            className="form-check-label"
                            style={{ color: "black" }}
                          >
                            <input
                              type="checkbox"
                              className="form-check-input"
                              disabled={!hasWebApp}
                              onChange={handleCheckVue}
                              checked={hasVue}
                            />
                            Vue
                            <i className="input-helper"></i>
                          </label>
                        </li>
                        <li className="form-check form-check-flat form-check-primary">
                          <label
                            className="form-check-label"
                            style={{ color: "black" }}
                          >
                            <input
                              type="checkbox"
                              className="form-check-input"
                              disabled={!hasWebApp}
                              onChange={handleCheckWordPress}
                              checked={hasWordPress}
                            />
                            WordPress
                            <i className="input-helper"></i>
                          </label>
                        </li>
                      </ul>
                    </p>
                  </div>
                </div>
                <div className="form-check form-check-flat form-check-primary">
                  <p className="form-check-label" style={{ color: "black" }}>
                    <input
                      type="checkbox"
                      className="form-check-input"
                      onChange={handleCheckBoxMobileApp}
                    />
                    Mobile App
                    <i className="input-helper"></i>
                  </p>
                  <div className="form-check form-check-flat form-check-primary">
                    <p className="form-check-label" style={{ color: "black" }}>
                      <ul style={{ display: "flex", gap: "30px" }}>
                        <li className="form-check form-check-flat form-check-primary">
                          <label
                            className="form-check-label"
                            style={{ color: "black" }}
                          >
                            <input
                              type="checkbox"
                              className="form-check-input"
                              disabled={!hasMobileApp}
                              onChange={handleCheckNativeAndroid}
                              checked={hasNativeAndroid}
                            />
                            Native Android
                            <i className="input-helper"></i>
                          </label>
                        </li>
                        <li className="form-check form-check-flat form-check-primary">
                          <label
                            className="form-check-label"
                            style={{ color: "black" }}
                          >
                            <input
                              type="checkbox"
                              className="form-check-input"
                              disabled={!hasMobileApp}
                              onChange={handleCheckNativeIos}
                              checked={hasNativeIos}
                            />
                            Native IOS
                            <i className="input-helper"></i>
                          </label>
                        </li>
                        <li className="form-check form-check-flat form-check-primary">
                          <label
                            className="form-check-label"
                            style={{ color: "black" }}
                          >
                            <input
                              type="checkbox"
                              className="form-check-input"
                              disabled={!hasMobileApp}
                              onChange={handleCheckFlutter}
                              checked={hasFlutter}
                            />
                            Flutter
                            <i className="input-helper"></i>
                          </label>
                        </li>
                      </ul>
                    </p>
                  </div>
                </div>
                <div className="form-check form-check-flat form-check-primary">
                  <p className="form-check-label" style={{ color: "black" }}>
                    <input
                      type="checkbox"
                      className="form-check-input"
                      onChange={handleCheckBoxAPI}
                    />
                    API
                    <i className="input-helper"></i>
                  </p>
                  <div className="form-check form-check-flat form-check-primary">
                    <p className="form-check-label" style={{ color: "black" }}>
                      <ul style={{ display: "flex", gap: "30px" }}>
                        <li className="form-check form-check-flat form-check-primary">
                          <label
                            className="form-check-label"
                            style={{ color: "black" }}
                          >
                            <input
                              type="checkbox"
                              className="form-check-input"
                              disabled={!hasApiTechnology}
                              onChange={handleCheckAPIWordPress}
                              checked={hasAPIWordPress}
                            />
                            WordPress
                            <i className="input-helper"></i>
                          </label>
                        </li>
                        <li className="form-check form-check-flat form-check-primary">
                          <label
                            className="form-check-label"
                            style={{ color: "black" }}
                          >
                            <input
                              type="checkbox"
                              className="form-check-input"
                              disabled={!hasApiTechnology}
                              onChange={handleCheckAPINodejs}
                              checked={hasAPINodejs}
                            />
                            Node js
                            <i className="input-helper"></i>
                          </label>
                        </li>
                        <li className="form-check form-check-flat form-check-primary">
                          <label
                            className="form-check-label"
                            style={{ color: "black" }}
                          >
                            <input
                              type="checkbox"
                              className="form-check-input"
                              disabled={!hasApiTechnology}
                              onChange={handleCheckLaravel}
                              checked={hasLaravel}
                            />
                            Laravel
                            <i className="input-helper"></i>
                          </label>
                        </li>
                      </ul>
                    </p>
                  </div>
                </div>
                <div className="form-group">
                  <div className="form-check">
                    <p className="form-check-label" style={{ color: "black" }}>
                      <input
                        type="checkbox"
                        className="form-check-input"
                        onChange={handleCheckBoxBudget}
                      />
                      Budget
                      <i className="input-helper"></i>
                    </p>
                    <div
                      className="form-group"
                      style={{
                        display: "flex",
                        marginRight: "450px",
                        gap: "20px",
                      }}
                    >
                      <input
                        type="text"
                        className="form-control"
                        id="exampleInputPassword4"
                        placeholder="Minimum"
                        onChange={getdata}
                        name="minBudgetData"
                        disabled={!budget}
                        onKeyPress={(e) =>
                          !/[0-9]/.test(e.key) && e.preventDefault()
                        }
                        value={inpval.minBudgetData}
                      />
                      <input
                        type="text"
                        className="form-control"
                        id="exampleInputPassword4"
                        placeholder="Maximum"
                        onChange={getdata}
                        name="maxBudgetData"
                        disabled={!budget}
                        onKeyPress={(e) =>
                          !/[0-9]/.test(e.key) && e.preventDefault()
                        }
                        value={inpval.maxBudgetData}
                      />

                    </div>
                  </div>
                </div>
                <div className="form-group">
                  <i style={{ color: "red", marginBottom: "10px" }}>

                    {validation.maxBudgetData}  </i>
                </div>
                <div className="form-group">
                  <div className="form-check">
                    <p className="form-check-label" style={{ color: "black" }}>
                      <input
                        type="checkbox"
                        className="form-check-input"
                        onChange={handleCheckBoxTimeline}
                      />
                      Timeline{" "}
                      <i className="input-helper">
                        <span style={{ color: "black" }}>(In Hours)</span>
                      </i>
                    </p>
                    <div
                      className="form-group"
                      style={{
                        display: "flex",
                        marginRight: "450px",
                        gap: "20px",
                      }}
                    >
                      <input
                        type="text"
                        className="form-control"
                        id="exampleInputPassword4"
                        placeholder="Min Time"
                        onChange={getdata}
                        name="minTimelineData"
                        disabled={!hasTimeline}
                        onKeyPress={(e) =>
                          !/[0-9]/.test(e.key) && e.preventDefault()
                        }
                        value={inpval.minTimelineData}
                      />
                      <input
                        type="text"
                        className="form-control"
                        id="exampleInputPassword4"
                        placeholder="Max Time"
                        onChange={getdata}
                        name="maxTimelineData"
                        disabled={!hasTimeline}
                        onKeyPress={(e) =>
                          !/[0-9]/.test(e.key) && e.preventDefault()
                        }
                        value={inpval.maxTimelineData}
                      />

                    </div>

                  </div>

                </div>
                <div className="form-group">
                  <i style={{ color: "red", marginBottom: "10px" }}>

                    {validation.maxTimelineData} </i>
                </div>
                <button onClick={addData} className="btn btn-primary me-2">
                  Submit
                </button>
                <button onClick={reset} className="btn btn-light">Reset</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default FormValidation;
