import { useContext } from "react";
import { Accordion, Col, Container, Form, Row } from "react-bootstrap";
import Modal from "react-bootstrap/Modal";
import { LeadContext } from "../../../context/LeadContext";
import Details from "./Details";
import ModelDropdowns from "./ModelDropdowns";
import ModelTabsInformation from "./ModelTabsInformation";

function ProjectModelDetails(props) {
  const visible = props.visible;
  const item = useContext(LeadContext)
  // console.log(item)
  // console.log(props)
  const history = props.history;
  const handleClose = () => {
    props.closeModel()
  };

  console.log(item)
  return (
    <>
      <Container>
        <Modal
          dialogClassName="modal-90w"
          aria-labelledby="contained-modal-title-vcenter"
          centered
          show={visible}
          size="lg"
          backdrop="static"
          keyboard={false}
          onHide={handleClose}

        >
          <Modal.Header closeButton>
            <Modal.Title>{item.project_name}-{item.client_name}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Row>
              <Col>
                <div style={{ height: "800px", overflowX: "scroll" }} tabIndex={0}>
                  <Form >
                    <Form.Label>{item.client_name}</Form.Label> <br />
                    <Form.Label className="text-dark">Activity</Form.Label>
                  </Form>
                  <ModelTabsInformation />
                </div>

              </Col>
              <Col>
                <Row>
                  <Col>
                    {" "}
                    <ModelDropdowns selectDropdownHandler={props.selectDropdownHandler} columns={props.columns} /> <br />
                  </Col>
                  <Row>
                    <Accordion defaultActiveKey="1">
                      <Accordion.Item eventKey="1">
                        <Accordion.Header>Accounts</Accordion.Header>
                        <Accordion.Body>
                          <Row>
                            <Col>
                              <Form>
                                <Form.Group className="mb-3">
                                  <Form.Label className="text-dark">
                                    Client Name
                                  </Form.Label>
                                  <br />
                                  <Form.Label className="text-dark">
                                    Project Name
                                  </Form.Label>
                                </Form.Group>
                              </Form>
                            </Col>
                            <Col>
                              <Form>
                                <Form.Group className="mb-3">
                                  <Form.Label className="text-dark">
                                    {item.client_name}
                                  </Form.Label>
                                  <br />
                                  <Form.Label className="text-dark">
                                    {item.project_name}
                                  </Form.Label>
                                </Form.Group>
                              </Form>
                            </Col>
                          </Row>
                        </Accordion.Body>
                      </Accordion.Item>
                    </Accordion>

                    <Accordion defaultActiveKey="0">
                      <Accordion.Item eventKey="0">
                        <Accordion.Header>Details</Accordion.Header>
                        <Accordion.Body>
                          <Row>
                            <Details />
                          </Row>
                        </Accordion.Body>
                      </Accordion.Item>
                    </Accordion>
                  </Row>
                </Row>
                <Row>
                  <Form>
                    <Form.Group className="mb-3">
                      <Form.Label className="text-dark" style={{ marginTop: "60px" }}>
                        {history.event_name}
                      </Form.Label>
                      <br />
                      <Form.Label className="text-dark">
                        {history.user}
                      </Form.Label>
                      <br />
                      <Form.Label className="text-dark">
                        {history.event_date}
                      </Form.Label>
                    </Form.Group>
                  </Form>
                </Row>
              </Col>
            </Row>
          </Modal.Body>
          <Modal.Footer>
            <button className="btn btn-danger btn-sm" onClick={handleClose}>
              Close
            </button>
          </Modal.Footer>
        </Modal>
      </Container>
    </>
  );
}
export default ProjectModelDetails;

