// import axios from "axios";
import { useContext, useEffect, useState } from "react"
import { LeadContext, LeadDispatchContext } from "../../../context/LeadContext";
// import { BASE_URL } from "../../../util/constants";
import { instance } from "../../../util/interceptor";
const Details = () => {
    const item = useContext(LeadContext)
    const setLeadDetails = useContext(LeadDispatchContext)
    const [hasDesign, setHasDesign] = useState(item.hasDesign);
    const [hasWebsite, setHasWebsite] = useState(item.hasWebsite);
    const [hasWebApp, setHasWebApp] = useState(item.hasWebApp);
    const [hasMobileApp, setHasMobileApp] = useState(item.hasMobileApp);
    const [hasApiTechnology, setHasAPI] = useState(item.hasApiTechnology);
    const [budget, setBudget] = useState(item.budget);
    const [hasTimeline, setHasTimeline] = useState(item.hasTimeline);
    const [hasReact, setHasReact] = useState(false);
    const [hasAngular, setHasAngular] = useState(false);
    const [hasVue, setHasVue] = useState(false);
    const [hasWordPress, setHasWordPress] = useState(false);
    const [hasNativeAndroid, setHasNativeAndroid] = useState(false);
    const [hasNativeIos, setHasNativeIos] = useState(false);
    const [hasFlutter, setHasFlutter] = useState(false);
    const [hasAPIWordPress, setHasAPIWordPress] = useState(false);
    const [hasAPINodejs, setHasAPINodejs] = useState(false);
    const [hasLaravel, setHasLaravel] = useState(false);
    const [inpval, setInpval] = useState({
        client_name: item.client_name,
        project_name: item.project_name,
        web_technology: [],
        mobile_technology: [],
        API_technology: [],
        client_email: item.client_email,
        client_phone: item.client_phone,
        client_skype: item.client_skype,
        client_location: item.client_location,
        project_location: item.project_location,
        project_description: item.project_description,
        hasDesign: item.hasDesign,
        designData: item.designData,
        hasWebsite: item.hasWebsite,
        websiteData: item.websiteData,
        hasWebApp: item.hasWebApp,
        hasMobileApp: item.hasMobileApp,
        hasApiTechnology: item.hasApiTechnology,
        budget: item.budget,
        minBudgetData: item.minBudgetData,
        maxBudgetData: item.maxBudgetData,
        hasTimeline: item.hasTimeline,
        minTimelineData: item.minTimelineData,
        maxTimelineData: item.maxTimelineData,
        hasReact: hasReact,
        hasAngular: hasAngular,
        hasVue: hasVue,
        hasWordPress: hasWordPress,
        hasNativeAndroid: hasNativeAndroid,
        hasNativeIos: hasNativeIos,
        hasFlutter: hasFlutter,
        hasAPIWordPress: hasAPIWordPress,
        hasAPINodejs: hasAPINodejs,
        hasLaravel: hasLaravel,


    });
    const [validation, setValidation] = useState({
        client_name: "",
        client_email: "",
        client_phone: "",
        client_skype: "",
        client_location: "",
        project_name: "",
        project_location: "",
        project_description: "",
        hasDesign: hasDesign,
        designData: "",
        hasWebsite: hasWebsite,
        websiteData: "",
        hasWebApp: hasWebApp,
        hasMobileApp: hasMobileApp,
        hasApiTechnology: hasApiTechnology,
        budget: budget,
        minBudgetData: "",
        maxBudgetData: "",
        hasTimeline: hasTimeline,
        minTimelineData: "",
        maxTimelineData: "",
        web_technology: [],
        mobile_technology: [],
        API_technology: [],
    });
    useEffect(() => {
        if (item.hasWebApp === true) {
            if (item.web_technology.length !== 0) {
                item.web_technology.map((res) => {
                    if (res === "React") {
                        setHasReact(true)
                        setInpval((previousState) => {
                            return {
                                ...previousState,
                                web_technology: item.web_technology,
                            };
                        });
                    }
                    if (res === "Angular") {
                        setHasAngular(true)
                        setInpval((previousState) => {
                            return {
                                ...previousState,
                                web_technology: item.web_technology,
                            };
                        });
                    }
                    if (res === "Vue") {
                        setHasVue(true)
                        setInpval((previousState) => {
                            return {
                                ...previousState,
                                web_technology: item.web_technology,
                            };
                        });
                    }
                    if (res === "WordPress") {
                        setHasWordPress(true)
                        setInpval((previousState) => {
                            return {
                                ...previousState,
                                web_technology: item.web_technology,
                            };
                        });
                    }
                })



            }


        }
        if (item.hasMobileApp === true) {
            if (item.mobile_technology.length !== 0) {
                item.mobile_technology.map((res) => {
                    if (res === "NativeAndroid") {
                        setHasNativeAndroid(true)
                        setInpval((previousState) => {
                            return {
                                ...previousState,
                                mobile_technology: item.mobile_technology,
                            };
                        });
                    }
                    if (res === "NativeIOS") {
                        setHasNativeIos(true)
                        setInpval((previousState) => {
                            return {
                                ...previousState,
                                mobile_technology: item.mobile_technology,
                            };
                        });
                    }
                    if (res === "Flutter") {
                        setHasFlutter(true)
                        setInpval((previousState) => {
                            return {
                                ...previousState,
                                mobile_technology: item.mobile_technology,
                            };
                        });
                    }

                })



            }

        }
        if (item.hasApiTechnology === true) {
            if (item.API_technology.length !== 0) {
                item.API_technology.map((res) => {
                    if (res === "WordPress") {
                        setHasAPIWordPress(true)
                        setInpval((previousState) => {
                            return {
                                ...previousState,
                                API_technology: item.API_technology,
                            };
                        });

                    }
                    if (res === "Laravel") {
                        setHasLaravel(true)
                        setInpval((previousState) => {
                            return {
                                ...previousState,
                                API_technology: item.API_technology,
                            };
                        });
                    }
                    if (res === "Node js") {
                        setHasAPINodejs(true)
                        setInpval((previousState) => {
                            return {
                                ...previousState,
                                API_technology: item.API_technology,
                            };
                        });
                    }

                })



            }

        }

    }, [item])
    const id = item.id
    // if(item.web_technology.length!==0){
    //     item.web_technology.map((res)=>{
    //         if(res==="React"){
    //             setHasReact((prevState)=>!prevState)
    //         }
    //         if(res==="Angular"){
    //             setHasAngular((prevState)=>!prevState)
    //         }
    //         if(res==="Vue"){
    //             setHasVue((prevState)=>!prevState)
    //         }
    //         if(res==="WordPress"){
    //             setHasWordPress((prevState)=>!prevState)
    //         }
    //     })   



    // }
    const handleCheckBoxDesign = (e) => {
        if (e.target.checked) {
            setHasDesign((prevState) => !prevState);

            setInpval((previousState) => {
                return { ...previousState, hasDesign: !hasDesign };
            });
        } else {
            setHasDesign((prevState) => !prevState);
            setInpval((previousState) => {
                return { ...previousState, hasDesign: !hasDesign, designData: "" };
            });

            setValidation((previousState) => {
                return { ...previousState, designData: "" };
            });
        }
    };

    const handleCheckBoxWebsite = (e) => {
        if (e.target.checked) {
            setHasWebsite((prevState) => !prevState);

            setInpval((previousState) => {
                return { ...previousState, hasWebsite: !hasWebsite };
            });
        } else {
            setHasWebsite((prevState) => !prevState);
            setInpval((previousState) => {
                return { ...previousState, hasWebsite: !hasWebsite, websiteData: "" };
            });

            setValidation((previousState) => {
                return { ...previousState, websiteData: "" };
            });
        }
    };
    const handleCheckBoxWebApp = (e) => {
        if (e.target.checked) {
            setHasWebApp((prevState) => !prevState);
            setHasReact((prevState) => prevState);
            setHasAngular((prevState) => prevState);
            setHasVue((prevState) => prevState);
            setHasWordPress((prevState) => prevState);
            setInpval((previousState) => {
                return { ...previousState, hasWebApp: !hasWebApp };
            });
        } else {
            setHasReact((prevState) => false);
            setHasAngular((prevState) => false);
            setHasVue((prevState) => false);
            setHasWordPress((prevState) => false);
            setHasWebApp((prevState) => !prevState);
        }
    };
    const handleCheckBoxMobileApp = (e) => {
        if (e.target.checked) {
            setHasMobileApp((prevState) => !prevState);
            setHasNativeAndroid((prevState) => prevState);
            setHasNativeIos((prevState) => prevState);
            setHasFlutter((prevState) => prevState);

            setInpval((previousState) => {
                return { ...previousState, hasMobileApp: !hasMobileApp };
            });
        } else {
            setHasNativeAndroid((prevState) => false);
            setHasNativeIos((prevState) => false);
            setHasFlutter((prevState) => false);
            setHasMobileApp((prevState) => !prevState);
        }
    };
    const handleCheckBoxAPI = (e) => {
        if (e.target.checked) {
            setHasAPI((prevState) => !prevState);
            setHasAPIWordPress((prevState) => prevState);
            setHasAPINodejs((prevState) => prevState);
            setHasLaravel((prevState) => prevState);

            setInpval((previousState) => {
                return { ...previousState, hasApiTechnology: !hasApiTechnology };
            });
        } else {
            setHasAPI((prevState) => false);
            setHasAPIWordPress((prevState) => false);
            setHasAPINodejs((prevState) => false);
            setHasLaravel((prevState) => false);
        }
    };
    const handleCheckBoxBudget = (e) => {
        if (e.target.checked) {
            setBudget((prevState) => !prevState);
            setInpval((previousState) => {
                return { ...previousState, budget: !budget };
            });
        } else {
            setBudget((prevState) => false);
            setInpval((previousState) => {
                return { ...previousState, maxBudgetData: "", minBudgetData: "" };
            });
        }
    };
    const handleCheckBoxTimeline = (e) => {
        if (e.target.checked) {
            setHasTimeline((prevState) => !prevState);
            setInpval((previousState) => {
                return { ...previousState, hasTimeline: !hasTimeline };
            });
        } else {
            setHasTimeline((prevState) => false);
            setInpval((previousState) => {
                return { ...previousState, maxTimelineData: "", minTimelineData: "" };
            });
        }
    };

    const handleCheckReact = (event) => {
        if (event.target.checked) {
            setHasReact((prevState) => !prevState);

            setInpval((previousState) => {
                return {
                    ...previousState,
                    web_technology: [...previousState.web_technology, "React"],
                };
            });
        } else {
            setHasReact((prevState) => !prevState);
            setInpval((previousState) => {
                return {
                    ...previousState,
                    web_technology: previousState.web_technology.filter(
                        (x) => x === !"React"
                    ),
                };
            });
        }
    };
    const handleCheckAngular = (event) => {
        if (event.target.checked) {
            setHasAngular((prevState) => !prevState);

            setInpval((previousState) => {
                return {
                    ...previousState,
                    web_technology: [...previousState.web_technology, "Angular"],
                };
            });
        } else {
            setHasAngular((prevState) => !prevState);

            setInpval((previousState) => {
                return {
                    ...previousState,
                    web_technology: previousState.web_technology.filter(
                        (x) => x === !"Angular"
                    ),
                };
            });
        }
    };
    const handleCheckVue = (event) => {
        if (event.target.checked) {
            setHasVue((prevState) => !prevState);

            setInpval((previousState) => {
                return {
                    ...previousState,
                    web_technology: [...previousState.web_technology, "Vue"],
                };
            });
        } else {
            setHasVue((prevState) => !prevState);
            setInpval((previousState) => {
                return {
                    ...previousState,
                    web_technology: previousState.web_technology.filter(
                        (x) => x === !"Vue"
                    ),
                };
            });
        }
    };
    const handleCheckWordPress = (event) => {
        if (event.target.checked) {
            setHasWordPress((prevState) => !prevState);

            setInpval((previousState) => {
                return {
                    ...previousState,
                    web_technology: [...previousState.web_technology, "WordPress"],
                };
            });
        } else {
            setHasWordPress((prevState) => !prevState);

            setInpval((previousState) => {
                return {
                    ...previousState,
                    web_technology: previousState.web_technology.filter(
                        (x) => x === !"WordPress"
                    ),
                };
            });
        }
    };
    const handleCheckNativeAndroid = (event) => {
        if (event.target.checked) {
            setHasNativeAndroid((prevState) => !prevState);

            setInpval((previousState) => {
                return {
                    ...previousState,
                    mobile_technology: [
                        ...previousState.mobile_technology,
                        "NativeAndroid",
                    ],
                };
            });
        } else {
            setInpval((previousState) => {
                return {
                    ...previousState,
                    mobile_technology: previousState.mobile_technology.filter(
                        (x) => x === !"NativeAndroid"
                    ),
                };
            });
        }
    };
    const handleCheckNativeIos = (event) => {
        if (event.target.checked) {
            setHasNativeIos((prevState) => !prevState);

            setInpval((previousState) => {
                return {
                    ...previousState,
                    mobile_technology: [...previousState.mobile_technology, "NativeIOS"],
                };
            });
        } else {
            setInpval((previousState) => {
                return {
                    ...previousState,
                    mobile_technology: previousState.mobile_technology.filter(
                        (x) => x === !"NativeIOS"
                    ),
                };
            });
        }
    };
    const handleCheckFlutter = (event) => {
        if (event.target.checked) {
            setHasFlutter((prevState) => !prevState);

            setInpval((previousState) => {
                return {
                    ...previousState,
                    mobile_technology: [...previousState.mobile_technology, "Flutter"],
                };
            });
        } else {
            setInpval((previousState) => {
                return {
                    ...previousState,
                    mobile_technology: previousState.mobile_technology.filter(
                        (x) => x === !"Flutter"
                    ),
                };
            });
        }
    };
    const handleCheckAPIWordPress = (event) => {
        if (event.target.checked) {
            setHasAPIWordPress((prevState) => !prevState);

            setInpval((previousState) => {
                return {
                    ...previousState,
                    API_technology: [...previousState.API_technology, "WordPress"],
                };
            });
        } else {
            setInpval((previousState) => {
                return {
                    ...previousState,
                    API_technology: previousState.API_technology.filter(
                        (x) => x === !"WordPress"
                    ),
                };
            });
        }
    };
    const handleCheckAPINodejs = (event) => {
        if (event.target.checked) {
            setHasAPINodejs((prevState) => !prevState);

            setInpval((previousState) => {
                return {
                    ...previousState,
                    API_technology: [...previousState.API_technology, "Node js"],
                };
            });
        } else {
            setInpval((previousState) => {
                return {
                    ...previousState,
                    API_technology: previousState.API_technology.filter(
                        (x) => x === !"Node js"
                    ),
                };
            });
        }
    };
    const handleCheckLaravel = (event) => {
        if (event.target.checked) {
            setHasLaravel((prevState) => !prevState);

            setInpval((previousState) => {
                return {
                    ...previousState,
                    API_technology: [...previousState.API_technology, "Laravel"],
                };
            });
        } else {
            setInpval((previousState) => {
                return {
                    ...previousState,
                    API_technology: previousState.API_technology.filter(
                        (x) => x === !"Laravel"
                    ),
                };
            });
        }
    };

    const getdata = (e) => {
        const { value, name } = e.target;

        setInpval(() => {
            return {
                ...inpval,
                [name]: value,
            };
        });

        if (e.target.name === "client_phone") {
            if (e.target.value.length < 4) {
                setValidation((prevState) => {
                    return {
                        ...prevState,
                        client_phone: "minimum 4 digits required",
                    };
                });
            } else if (e.target.value >= 4 && e.target.value.length <= 12) {
                setValidation((prevState) => {
                    return {
                        ...prevState,
                        client_phone: "",
                    };
                });
            } else if (e.target.value.length > 12) {
                setValidation((prevState) => {
                    return {
                        ...prevState,
                        client_phone: "maximum 12 digits allowed",
                    };
                });
            }
        }
    };

    const saveChangesHandler = async (e) => {
        e.preventDefault();
        const {
            client_name,
            client_email,
            project_name,
            hasDesign,
            hasWebsite,
            designData,
            websiteData,
            minBudgetData,
            maxBudgetData,
            minTimelineData,
            maxTimelineData,
        } = inpval;

        if (!client_name) {
            setValidation((prevState) => {
                return {
                    ...prevState,
                    client_name: " client name is required",
                };
            });
        }


        if (client_email && !client_email.includes("@")) {
            setValidation((prevState) => {
                return {
                    ...prevState,
                    client_email: "client email must have @ ",
                };
            });
        }
        if (!project_name) {
            setValidation((prevState) => {
                return {
                    ...prevState,
                    project_name: "project name is required",
                };
            });
        }


        if (hasDesign === true && designData === "") {
            setValidation((prevState) => {
                return {
                    ...prevState,
                    designData: "design must be fill",
                };
            });
        }
        if (hasDesign === true && designData !== "") {
            setValidation((prevState) => {
                return {
                    ...prevState,
                    designData: "",
                };
            });
        }
        if (hasWebsite === true && websiteData === "") {
            setValidation((prevState) => {
                return {
                    ...prevState,
                    websiteData: "website must be fill",
                };
            });
        }
        if (hasWebsite === true && websiteData !== "") {
            setValidation((prevState) => {
                return {
                    ...prevState,
                    websiteData: "",
                };
            });
        }

        if (budget === true && maxBudgetData < minBudgetData) {
            setValidation((prevState) => {
                return {
                    ...prevState,
                    maxBudgetData:
                        "(maximum budget sholuld be greater than or equal to  minimum budget)",
                };
            });
        }
        if (budget === true && maxBudgetData > minBudgetData) {
            setValidation((prevState) => {
                return {
                    ...prevState,
                    maxBudgetData: "",
                };
            });
        }

        if (hasTimeline === true && maxTimelineData < minTimelineData) {
            setValidation((prevState) => {
                return {
                    ...prevState,
                    maxTimelineData:
                        "(maximum time sholuld be greater than or equal to minimum time)",
                };
            });
        }
        if (hasTimeline === true && maxTimelineData > minTimelineData) {
            setValidation((prevState) => {
                return {
                    ...prevState,
                    maxTimelineData: "",
                };
            });
        }
        else {
            // const token = JSON.parse(localStorage.getItem("userlogin")).access.token;
            // const headers = {
            //     Authorization: `Bearer ${token}`,
            // };
            instance
                .patch(
                    // `${BASE_URL}/v1/lead/updatelead/${id}`,
                    `/v1/lead/updatelead/${id}`,


                    {
                        ...inpval,
                    },
                    {
                        headers: {
                            Authorization: `Bearer ${JSON.parse(localStorage.getItem("userlogin")).access.token}`,
                        }
                    }
                )
                .then((data) => {
                    alert("You have successfully updated your details")
                    setLeadDetails({ ...data.data.result })
                })
                .catch((error) => {
                    console.log(error);
                });
        }
    };
    console.log(item)

    return (
        <>
            <div className="row">
                <div className="col grid-margin stretch-card">
                    <div className="card">
                        <div className="card-body">
                            <form className="forms-sample row">
                                <div className="form-group col-md-6">
                                    <label htmlFor="exampleInputName1">Client Name</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        name="client_name"
                                        placeholder="Client Name"
                                        onChange={getdata}
                                        // disabled={!hasEditDetails}
                                        value={inpval.client_name}
                                    />
                                    <p style={{ color: "red", marginBottom: "10px" }}>
                                        {validation.client_name}
                                    </p>
                                </div>

                                <div className="form-group col-md-6">
                                    <label htmlFor="exampleInputEmail3">Client Email </label>
                                    <input
                                        type="email"
                                        className="form-control"
                                        name="client_email"
                                        placeholder="Client Email"
                                        onChange={getdata}
                                        value={inpval.client_email}
                                    />
                                    <p style={{ color: "red", marginBottom: "10px" }}>
                                        {validation.client_email}
                                    </p>
                                </div>
                                <div className="form-group col-md-6">
                                    <label htmlFor="exampleInputEmail3">Client Phone </label>
                                    <input
                                        value={inpval.client_phone}
                                        className="form-control"
                                        name="client_phone"
                                        placeholder="Client Phone"
                                        onChange={getdata}
                                        onKeyPress={(e) =>
                                            !/[0-9]/.test(e.key) && e.preventDefault()
                                        }
                                    />
                                    <p style={{ color: "red", marginBottom: "10px" }}>
                                        {validation.client_phone}
                                    </p>
                                </div>
                                <div className="form-group col-md-6">
                                    <label htmlFor="exampleInputEmail3">Client Skype </label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        name="client_skype"
                                        placeholder="Client Skype"
                                        onChange={getdata}
                                        value={inpval.client_skype}
                                    />
                                </div>
                                <div className="form-group col-md-6">
                                    <label htmlFor="exampleInputPassword4">Client Location</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        name="client_location"
                                        placeholder="Client Location"
                                        onChange={getdata}
                                        value={inpval.client_location}
                                    />
                                    <p style={{ color: "red", marginBottom: "10px" }}>
                                        {validation.client_location}
                                    </p>
                                </div>
                                <div className="form-group col-md-6">
                                    <label htmlFor="exampleInputEmail3">Project Name</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        name="project_name"
                                        placeholder="Project Name"
                                        onChange={getdata}
                                        value={inpval.project_name}
                                    />
                                    <p style={{ color: "red", marginBottom: "10px" }}>
                                        {validation.project_name}
                                    </p>
                                </div>

                                <div className="form-group col-md-6">
                                    <label htmlFor="exampleInputPassword4">Project Location</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        name="project_location"
                                        placeholder="Project Location"
                                        onChange={getdata}
                                        value={inpval.project_location}
                                    />
                                    <p style={{ color: "red", marginBottom: "10px" }}>
                                        {validation.project_location}
                                    </p>
                                </div>
                                <div className="form-group col-md-6">
                                    <label htmlFor="exampleInputPassword4">Project Description</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        name="project_description"
                                        placeholder="Project Description"
                                        onChange={getdata}
                                        value={inpval.project_description}
                                    />
                                    <p style={{ color: "red", marginBottom: "10px" }}>
                                        {validation.project_description}
                                    </p>
                                </div>
                                <div className="form-group">
                                    <div className="form-check">
                                        <p className="form-check-label" style={{ color: "black" }}>
                                            <input
                                                type="checkbox"
                                                className="form-check-input"
                                                onChange={handleCheckBoxDesign}
                                                name="hasDesign"
                                                checked={hasDesign}

                                            />
                                            Design Provided?
                                            <i className="input-helper"></i>
                                        </p>
                                        <div className="form-group">
                                            <textarea
                                                type="text"
                                                className="form-control"
                                                id="exampleInputPassword4"
                                                disabled={!hasDesign}
                                                placeholder="Design"
                                                name="designData"
                                                value={inpval.designData}
                                                onChange={getdata}
                                            />
                                            <p style={{ color: "red", marginBottom: "10px" }}>
                                                {validation.designData}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <div className="form-check">
                                        <p className="form-check-label" style={{ color: "black" }}>
                                            <input
                                                type="checkbox"
                                                className="form-check-input"
                                                name="hasWebsite"
                                                onChange={handleCheckBoxWebsite}
                                                checked={hasWebsite}
                                            />
                                            Website/Mobile App Refrences?
                                            <i className="input-helper"></i>
                                        </p>
                                        <div className="form-group">
                                            <input
                                                type="text"
                                                className="form-control"
                                                id="exampleInputPassword4"
                                                disabled={!hasWebsite}
                                                placeholder="Website"
                                                onChange={getdata}
                                                value={inpval.websiteData}
                                                name="websiteData"
                                            />
                                            <p style={{ color: "red", marginBottom: "10px" }}>
                                                {validation.websiteData}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <label style={{ color: "black" }}>Technology</label>
                                <div className="form-check form-check-flat form-check-primary">
                                    <p className="form-check-label" style={{ color: "black" }}>
                                        <input
                                            type="checkbox"
                                            className="form-check-input"
                                            onChange={handleCheckBoxWebApp}
                                            checked={hasWebApp}
                                        />{" "}
                                        Web App
                                        <i className="input-helper"></i>
                                    </p>
                                    <div className="form-check form-check-flat form-check-primary">
                                        <p className="form-check-label" style={{ color: "black" }}>
                                            <ul style={{ display: "flex", gap: "30px" }}>
                                                <li className="form-check form-check-flat form-check-primary">
                                                    <label
                                                        className="form-check-label"
                                                        style={{ color: "black" }}
                                                    >
                                                        <input
                                                            type="checkbox"
                                                            className="form-check-input"
                                                            disabled={!hasWebApp}
                                                            onChange={handleCheckReact}
                                                            checked={hasReact}
                                                        />
                                                        React
                                                        <i className="input-helper"></i>
                                                    </label>
                                                </li>
                                                <li className="form-check form-check-flat form-check-primary">
                                                    <label
                                                        className="form-check-label"
                                                        style={{ color: "black" }}
                                                    >
                                                        <input
                                                            type="checkbox"
                                                            className="form-check-input"
                                                            disabled={!hasWebApp}
                                                            onChange={handleCheckAngular}
                                                            checked={hasAngular}
                                                        />
                                                        Angular
                                                        <i className="input-helper"></i>
                                                    </label>
                                                </li>
                                                <li className="form-check form-check-flat form-check-primary">
                                                    <label
                                                        className="form-check-label"
                                                        style={{ color: "black" }}
                                                    >
                                                        <input
                                                            type="checkbox"
                                                            className="form-check-input"
                                                            disabled={!hasWebApp}
                                                            onChange={handleCheckVue}
                                                            checked={hasVue}
                                                        />
                                                        Vue
                                                        <i className="input-helper"></i>
                                                    </label>
                                                </li>
                                                <li className="form-check form-check-flat form-check-primary">
                                                    <label
                                                        className="form-check-label"
                                                        style={{ color: "black" }}
                                                    >
                                                        <input
                                                            type="checkbox"
                                                            className="form-check-input"
                                                            disabled={!hasWebApp}
                                                            onChange={handleCheckWordPress}
                                                            checked={hasWordPress}
                                                        />
                                                        WordPress
                                                        <i className="input-helper"></i>
                                                    </label>
                                                </li>
                                            </ul>
                                        </p>
                                    </div>
                                </div>
                                <div className="form-check form-check-flat form-check-primary">
                                    <p className="form-check-label" style={{ color: "black" }}>
                                        <input
                                            type="checkbox"
                                            className="form-check-input"
                                            onChange={handleCheckBoxMobileApp}
                                            checked={hasMobileApp}
                                        />
                                        Mobile App
                                        <i className="input-helper"></i>
                                    </p>
                                    <div className="form-check form-check-flat form-check-primary">
                                        <p className="form-check-label" style={{ color: "black" }}>
                                            <ul style={{ display: "flex", gap: "30px" }}>
                                                <li className="form-check form-check-flat form-check-primary">
                                                    <label
                                                        className="form-check-label"
                                                        style={{ color: "black" }}
                                                    >
                                                        <input
                                                            type="checkbox"
                                                            className="form-check-input"
                                                            disabled={!hasMobileApp}
                                                            onChange={handleCheckNativeAndroid}
                                                            checked={hasNativeAndroid}
                                                        />
                                                        Native Android
                                                        <i className="input-helper"></i>
                                                    </label>
                                                </li>
                                                <li className="form-check form-check-flat form-check-primary">
                                                    <label
                                                        className="form-check-label"
                                                        style={{ color: "black" }}
                                                    >
                                                        <input
                                                            type="checkbox"
                                                            className="form-check-input"
                                                            disabled={!hasMobileApp}
                                                            onChange={handleCheckNativeIos}
                                                            checked={hasNativeIos}
                                                        />
                                                        Native IOS
                                                        <i className="input-helper"></i>
                                                    </label>
                                                </li>
                                                <li className="form-check form-check-flat form-check-primary">
                                                    <label
                                                        className="form-check-label"
                                                        style={{ color: "black" }}
                                                    >
                                                        <input
                                                            type="checkbox"
                                                            className="form-check-input"
                                                            disabled={!hasMobileApp}
                                                            onChange={handleCheckFlutter}
                                                            checked={hasFlutter}
                                                        />
                                                        Flutter
                                                        <i className="input-helper"></i>
                                                    </label>
                                                </li>
                                            </ul>
                                        </p>
                                    </div>
                                </div>
                                <div className="form-check form-check-flat form-check-primary">
                                    <p className="form-check-label" style={{ color: "black" }}>
                                        <input
                                            type="checkbox"
                                            className="form-check-input"
                                            onChange={handleCheckBoxAPI}
                                            checked={hasApiTechnology}
                                        />
                                        API
                                        <i className="input-helper"></i>
                                    </p>
                                    <div className="form-check form-check-flat form-check-primary">
                                        <p className="form-check-label" style={{ color: "black" }}>
                                            <ul style={{ display: "flex", gap: "30px" }}>
                                                <li className="form-check form-check-flat form-check-primary">
                                                    <label
                                                        className="form-check-label"
                                                        style={{ color: "black" }}
                                                    >
                                                        <input
                                                            type="checkbox"
                                                            className="form-check-input"
                                                            disabled={!hasApiTechnology}
                                                            onChange={handleCheckAPIWordPress}
                                                            checked={hasAPIWordPress}
                                                        />
                                                        WordPress
                                                        <i className="input-helper"></i>
                                                    </label>
                                                </li>
                                                <li className="form-check form-check-flat form-check-primary">
                                                    <label
                                                        className="form-check-label"
                                                        style={{ color: "black" }}
                                                    >
                                                        <input
                                                            type="checkbox"
                                                            className="form-check-input"
                                                            disabled={!hasApiTechnology}
                                                            onChange={handleCheckAPINodejs}
                                                            checked={hasAPINodejs}
                                                        />
                                                        Node js
                                                        <i className="input-helper"></i>
                                                    </label>
                                                </li>
                                                <li className="form-check form-check-flat form-check-primary">
                                                    <label
                                                        className="form-check-label"
                                                        style={{ color: "black" }}
                                                    >
                                                        <input
                                                            type="checkbox"
                                                            className="form-check-input"
                                                            disabled={!hasApiTechnology}
                                                            onChange={handleCheckLaravel}
                                                            checked={hasLaravel}
                                                        />
                                                        Laravel
                                                        <i className="input-helper"></i>
                                                    </label>
                                                </li>
                                            </ul>
                                        </p>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <div className="form-check">
                                        <p className="form-check-label" style={{ color: "black" }}>
                                            <input
                                                type="checkbox"
                                                className="form-check-input"
                                                onChange={handleCheckBoxBudget}
                                            />
                                            Budget
                                            <i className="input-helper"></i>
                                        </p>
                                        <div className="row">
                                            <div
                                                className="col-6 grid-margin stretch-card"

                                            >
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    id="exampleInputPassword4"
                                                    placeholder="Maximum"
                                                    onChange={getdata}
                                                    name="maxBudgetData"
                                                    disabled={!budget}
                                                    onKeyDown={(e) =>
                                                        !/[0-9]/.test(e.key) && e.preventDefault()
                                                    }
                                                    value={inpval.maxBudgetData}
                                                />
                                            </div>
                                            <div className="col-6 grid-margin stretch-card">
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    id="exampleInputPassword4"
                                                    placeholder="Minimum"
                                                    onChange={getdata}
                                                    name="minBudgetData"
                                                    disabled={!budget}
                                                    onKeyDown={(e) =>
                                                        !/[0-9]/.test(e.key) && e.preventDefault()
                                                    }
                                                    value={inpval.minBudgetData}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <i style={{ color: "red", marginBottom: "10px" }}>

                                        {validation.maxBudgetData}  </i>
                                </div>
                                <div className="form-group">
                                    <div className="form-check">
                                        <p className="form-check-label" style={{ color: "black" }}>
                                            <input
                                                type="checkbox"
                                                className="form-check-input"
                                                onChange={handleCheckBoxTimeline}
                                            />
                                            Timeline{" "}
                                            <i className="input-helper">
                                                <span style={{ color: "black" }}>(In Hours)</span>
                                            </i>
                                        </p>
                                        <div className="row">
                                            <div className="col-6 grid-margin stretch-card">
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    id="exampleInputPassword4"
                                                    placeholder="Min Time"
                                                    onChange={getdata}
                                                    name="minTimelineData"
                                                    disabled={!hasTimeline}
                                                    onKeyDown={(e) =>
                                                        !/[0-9]/.test(e.key) && e.preventDefault()
                                                    }
                                                    value={inpval.minTimelineData}
                                                />
                                            </div>
                                            <div className="col-6 grid-margin stretch-card">
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    id="exampleInputPassword4"
                                                    placeholder="Max Time"
                                                    onChange={getdata}
                                                    name="maxTimelineData"
                                                    disabled={!hasTimeline}
                                                    onKeyDown={(e) =>
                                                        !/[0-9]/.test(e.key) && e.preventDefault()
                                                    }
                                                    value={inpval.maxTimelineData}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <i style={{ color: "red", marginBottom: "10px" }}>

                                        {validation.maxTimelineData} </i>
                                </div>
                                <div className="form-group">
                                    <button type="button" onClick={saveChangesHandler} className="btn btn-success btn-sm">
                                        Save Changes
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};
export default Details;
