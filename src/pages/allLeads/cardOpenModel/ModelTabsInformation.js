import axios from "axios";
import { useContext, useEffect, useState } from "react";
import Tab from "react-bootstrap/Tab";
import Tabs from "react-bootstrap/Tabs";
import Button from "react-bootstrap/Button";
// import { useNavigate } from "react-router-dom";
import { LeadContext } from "../../../context/LeadContext";
import { BASE_URL } from "../../../util/constants";
import { instance } from "../../../util/interceptor";
function ModelTabsInformation(props) {
  const [tab, setTab] = useState("comments");
  const [comment, setComment] = useState("");
  const [commentDetails, setCommentDetails] = useState([]);
  const [hasEdit, setHasEdit] = useState(false);
  const [commentId, setCommentId] = useState("");
  const [historyDetails, setHistoryDetails] = useState([])
  const [isHoveringEditId, setIsHoveringEditId] = useState("");
  const [isHoveringDeleteId, setIsHoveringDeleteId] = useState("");
  const [selectedFile, setSelectedFile] = useState(
    {
      image_file: null,
      image_preview: '',
    }
  )
  const item = useContext(LeadContext)
  const id = item.id;
  // console.log(id);
  // const navigate = useNavigate()

  useEffect(() => {
    fetchComments()
    fetchHistory()
  }, []);

  function fetchComments() {
    const url = `${BASE_URL}/v1/lead/getcomments/${id}`;
    // console.log(url);
    axios
      .get(url)
      .then((response) => {
        // console.log(response);
        setCommentDetails(response.data.result.comments);

      })
      .catch((response) => {
        // console.log(response);
        // navigate('/*')
      });
  }
  function fetchHistory() {
    const url = `${BASE_URL}/v1/lead/gethistory/${id}`;
    // console.log(url);
    axios
      .get(url)
      .then((response) => {
        // console.log(response.data.result.history);
        setHistoryDetails(response.data.result.history);
      })
      .catch((response) => {
        console.log(response);
      });
  }

  const handleComment = (event) => {
    setComment(event.target.value);
  };

  const commentEnterHandler = async (event) => {
    if (event.key === "Enter" && hasEdit === false) {
      event.preventDefault();

      // const url = `${BASE_URL}/v1/lead/createcomments/${id}`;
      const url = `/v1/lead/createcomments/${id}`;


      // const token = JSON.parse(localStorage.getItem("userlogin")).access.token;
      // const headers = {
      //   Authorization: `Bearer ${token}`,
      // };
      instance
        .post(
          url,
          {
            comments: comment,
          },
          { headers:  {
            Authorization: `Bearer ${JSON.parse(localStorage.getItem("userlogin")).access.token}`,
          } }
        )
        .then((response) => {
          // console.log(response.data.result.lead.comments);
          setCommentDetails(response.data.result.lead.comments);
        })
        .catch((response) => {
          console.log(response);
        });
    } else if (event.key === "Enter" && hasEdit === true) {
      event.preventDefault();
      // const url = `${BASE_URL}/v1/lead/updatecomments/${id}`;
      const url = `/v1/lead/updatecomments/${id}`;

      // console.log(url);
      // const token = JSON.parse(localStorage.getItem("userlogin")).access.token;
      // const headers = {
      //   Authorization: `Bearer ${token}`,
      // };
      instance
        .patch(
          url,
          {
            comments: comment,
            id: id,
          },
          { headers: {
            Authorization: `Bearer ${JSON.parse(localStorage.getItem("userlogin")).access.token}`,
          } }
        )
        .then((response) => {
          setCommentDetails(response.data.result.comments);
          setHasEdit((prevState) => !prevState);
        })
        .catch((response) => {
          console.log(response);
        });
    }
  };

  const createCommentHandler = async (event) => {
    event.preventDefault();

    // const url = `${BASE_URL}/v1/lead/createcomments/${id}`;
    const url = `/v1/lead/createcomments/${id}`;

    // console.log(url);
    // const token = JSON.parse(localStorage.getItem("userlogin")).access.token;
    // const headers = {
    //   Authorization: `Bearer ${token}`,
    // };
    comment.trim()
    if (
      comment.length !== 0
    ) {

      instance
        .post(
          url,
          {
            comments: comment,
          },
          { headers:  {
            Authorization: `Bearer ${JSON.parse(localStorage.getItem("userlogin")).access.token}`,
          } }
        )
        .then((response) => {
          // console.log(response.data.result.lead.comments);
          response.data.result.lead.comments.map((nextComment) => {
            if (
              nextComment.commented_text !== null &&
              nextComment.commented_text !== "" &&
              nextComment.commented_text !== undefined
            ) {
              return setCommentDetails(response.data.result.lead.comments);
            } else {
              return null
            }
          });

          setComment("");
        })
        .catch((response) => {
          console.log(response);
        });
    }

  };

  const onEditComment = (commented_text, editId) => {
    setComment(commented_text);
    setCommentId(editId);
    setHasEdit((prevState) => !prevState);
  };

  const updateCommentHandler = async () => {
    // const token = JSON.parse(localStorage.getItem("userlogin")).access.token;
    // const headers = {
    //   Authorization: `Bearer ${token}`,
    // };
    instance
      .patch(
        // `${BASE_URL}/v1/lead/updatecomments/${id}`,
        `/v1/lead/updatecomments/${id}`,

        {
          comments: comment,
          _id: commentId,
        },
        { headers: {
          Authorization: `Bearer ${JSON.parse(localStorage.getItem("userlogin")).access.token}`,
        } }
      )
      .then((response) => {
        response.data.result.comments.map((editComment) => {
          if (editComment._id === commentId) {
            editComment.commented_text = comment;
            setCommentDetails(response.data.result.comments);
            setComment("");
          }
          return null;
        });

        setHasEdit((prevState) => !prevState);
      })
      .catch((response) => {
        console.log(response);
      });
  };
  const deleteComment = async (deleteId) => {
    // setCommentId(deleteId)
    if (window.confirm("are you sure to delete this comment")) {

      // const token = JSON.parse(localStorage.getItem("userlogin")).access.token;
      // const headers = {
      //   Authorization: `Bearer ${token}`,
      // };
      instance
        .delete(
          // `${BASE_URL}/v1/lead/deletecomments/${id}/${deleteId}`,
          `/v1/lead/deletecomments/${id}/${deleteId}`,

          { headers: {
            Authorization: `Bearer ${JSON.parse(localStorage.getItem("userlogin")).access.token}`,
          } }
        )
        .then((response) => {
          // console.log(response.data.result.comments);
          response.data.result.comments.map((deleteComment) => {
            if (deleteComment._id === commentId) {
              deleteComment.commented_text = ""
            }
            return null;
          })
          setCommentDetails(response.data.result.comments);
          setComment("")
        })
        .catch((response) => {
          console.log(response);
        });
    }
    return
  };
  const handleMouseEnterEdit = (id) => {
    commentDetails.map((value) => {
      if (id === value._id) { setIsHoveringEditId(value._id); }
    })

  };

  const handleMouseLeaveEdit = () => {
    setIsHoveringEditId("");
  };
  const handleMouseEnterDelete = (id) => {
    commentDetails.map((value) => {
      if (id === value._id) { setIsHoveringDeleteId(value._id); }

    })


  };

  const handleMouseLeaveDelete = () => {
    setIsHoveringDeleteId("");
  };
  const imageHandler = (e) => {
    let image_as_base64 = URL.createObjectURL(e.target.files[0])
    let image_as_files = e.target.files[0];
    setSelectedFile(
      {
        image_preview: image_as_base64,
        image_file: image_as_files,
      }
    )
  }
  const handleUpload = (e) => {
    e.preventDefault()
    // const data = new FormData();
    // data.append("item", selectedFile.image_file);
    // console.log(data)
    // const url = `${BASE_URL}/v1/item/upload`;
    const url = `/v1/item/upload`;

    // console.log(e.target.files[0]);
    const token = JSON.parse(localStorage.getItem("userlogin")).access.token;
    // const headers = formData.getHeaders()
    // headers.Authorization= `Bearer ${token}`
    instance
      .post(
        url,
        {
          item: selectedFile.image_file
        },
        {
          headers: {
            "Authorization": `Bearer ${JSON.parse(localStorage.getItem("userlogin")).access.token}`,
            // "Content-type": "multipart/form-data",
            "Content-Type": "multipart/form-data",

          },
        }
      )
      .then((response) => {
        // console.log(response.data.result.itemPath[0])
        setComment(response.data.result.itemPath[0])
      })
      .catch((response) => {
        console.log(response);
      });
  }
  console.log(selectedFile)
  return (
    <Tabs
      activeKey={tab}
      id="justify-tab-example"
      className="mb-3"
      onSelect={(k) => setTab(k)}
    >
      <Tab eventKey="comments" title="Comments">
        <div className="card">
          <div className='"card-body'>
            <form className="forms-sample">
              <div className="form-group">
                <input
                  type="text"
                  placeholder="Add a comment..."
                  className="form-control"
                  name="comment"
                  value={comment}
                  onChange={handleComment}
                  onKeyDown={commentEnterHandler}
                />
                <br />
                {hasEdit ? (
                  <Button type="button" onClick={() => updateCommentHandler()}>
                    Update Comment
                  </Button>
                ) : (
                  <Button type="button" onClick={createCommentHandler}>
                    Create Comment
                  </Button>
                )}
                <input type="file" name="item" onChange={imageHandler}></input>
                <button onClick={handleUpload}>Upload</button>
                {/* <img src={selectedFile.image_preview} /> */}

              </div>
            </form>
            <img src={selectedFile.image_preview} />
            {commentDetails.map((nextComment) => {
              if (
                nextComment.commented_text !== null &&
                nextComment.commented_text !== "" &&
                nextComment.commented_text !== undefined
              ) {
                return (
                  <div className="row" key={nextComment._id}>
                    <div className="container"  >
                      <div className="row" style={{ color: "black" }}>{nextComment.commented_text}</div>
                      <br />
                      <div className="row">
                        <span
                          className="col-1"
                          onClick={() =>
                            onEditComment(
                              nextComment.commented_text,
                              nextComment._id
                            )
                          }
                          style={{ cursor: "pointer", color: "gray" }}
                        >
                          <b style={{ color: isHoveringEditId === nextComment._id ? 'blue' : 'gray' }}
                            onMouseEnter={() => handleMouseEnterEdit(nextComment._id)}
                            onMouseLeave={handleMouseLeaveEdit}
                          >Edit</b>
                        </span>
                        <span
                          className="col-2"
                          onClick={() => deleteComment(nextComment._id)}
                          style={{ cursor: "pointer" }}
                        >
                          <b style={{ color: isHoveringDeleteId === nextComment._id ? 'red' : 'gray' }}
                            onMouseEnter={() => handleMouseEnterDelete(nextComment._id)}
                            onMouseLeave={handleMouseLeaveDelete}
                          >Delete</b>
                        </span>
                        <br />
                      </div>
                      <div style={{ marginTop: "20px" }}></div>
                    </div>
                  </div>
                );
              } else {
                return null;
              }

            })}
          </div>
        </div>
      </Tab>
      <Tab eventKey="history" title="History">
        <div className="card">
          <div className='"card-body'>
            {historyDetails.map((value) => {
              return (
                <div className="container">
                  <div className="row" style={{ color: "black" }}>{value.event_name} by</div>
                  <div className="row" style={{ color: "black" }}>{value.userDetails.name}</div>
                  <div className="row" style={{ color: "black" }}> on {value.event_date}</div>

                </div>
              );

            })}


          </div>
        </div>
      </Tab>
    </Tabs>
  );
}

export default ModelTabsInformation;
