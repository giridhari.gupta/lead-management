import { useContext, useState } from 'react';
// import Dropdown from 'react-bootstrap/Dropdown';
import { LeadContext, LeadDispatchContext } from '../../../context/LeadContext';


function ModelDropdowns(props) {
  // const[source,setSource]=useState({
  //     droppableId:""

  // })
  // const[destination,setDestination]=useState({
  //     droppableId:""

  // })
  // const[draggableId,setDraggableId]=useState("")
  const [result, setResult] = useState({})
  // console.log(props)
  const setLeadDetails = useContext(LeadDispatchContext)
  let sourceIndex
  // const options=["Bid","Response","Meeting_Scheduled","Negociation","Waiting_for_Response","Win","Loose"]

  const leadDetails = useContext(LeadContext)
  // console.log(leadDetails)


  const onchangeHandler = (e) => {
    // console.log(props.columns)
    Object.entries(props.columns).map(([key, value], index) => {
      value.items.map((val, indexSource) => {
        if (val.id === leadDetails.id) {
          console.log(val)
          sourceIndex = indexSource
        }
      })

    });
    let newResult = { source: { droppableId: leadDetails.steps, index: sourceIndex }, destination: { droppableId: e.target.value, index: 0 }, draggableId: leadDetails.id }
    if (e.target.value !== "") {

      setResult(newResult)
      console.log(result)




    }
    props.selectDropdownHandler(newResult, props.columns)

  }
  console.log(leadDetails)

  // const steps=props.steps
  return (
    <select onChange={onchangeHandler}>
      <option selected={"Bid" === leadDetails.steps} value="Bid">Bid</option>
      <option selected={"Response" === leadDetails.steps} value="Response">Response</option>
      <option selected={"Meeting_Scheduled" === leadDetails.steps} value="Meeting_Scheduled">Meeting_Scheduled</option>
      <option selected={"Negociation" === leadDetails.steps} value="Negociation">Negociation</option>
      <option selected={"Waiting_for_Response" === leadDetails.steps} value="Waiting_for_Response">Waiting_for_Response</option>
      <option selected={"Win" === leadDetails.steps} value="Win">Win</option>
      <option selected={"Loose" === leadDetails.steps} value="Loose">Loose</option>
      {/* {options.map((value)=>{
          return(
            <option selected={value===leadDetails.steps}  value={value}>{value  }</option>

          )
        })} */}
    </select>
  );
}

export default ModelDropdowns;