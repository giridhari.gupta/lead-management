import { useState } from "react";
import Modal from "react-bootstrap/Modal";
import Input from './Input'
function ModelAdditionalDetails(props) {
    const [updatedAdditionalData, setUpdateAdditionalData] = useState()
    const visible = props.visible
    const field = props.field
    const[inpval,setInpval]=useState({budget:"",timeline:""})
    const handleClose = () => {
        props.handleCloseAdditional()

    }
    const updateAdditionalDataHandler = (data) => {
        setUpdateAdditionalData(data)

    }
    const onchangeHandler=(e)=>{
        const { value, name } = e.target
        setInpval(() => {
			return {
				...inpval,
				[name]: value,
			};
		});
        setUpdateAdditionalData([...field, ...field.filter((item) => {
            if (item.field_name === name) {
                item.answer = value
            }
        })])
    }
    const onSubmitHandler = (e) => {
        e.preventDefault()
//   updatedAdditionalData.filter((val)=>{
//     console.log("val -------",val.answer);
//     if( val.answer==="answer" ){
//         // if(val.answer!=="" ){
//         alert("must filled the required fields")

//     }
//     else{
//         props.onSubmit(updatedAdditionalData)

//     }
//   })
if(inpval.budget===""){
    alert("budget must be filled")
}
else if(inpval.timeline===""){
    alert("timeline must be filled")
}
else{
    props.onSubmit(updatedAdditionalData)

}

    }
   

// console.log(updatedAdditionalData)

    return (<>
        <Modal
            dialogClassName="modal-90w"
            aria-labelledby="contained-modal-title-vcenter"
            centered
            show={visible}
            size="lg"
            backdrop="static"
            keyboard={false}
            onHide={handleClose}

        >
            <Modal.Header closeButton></Modal.Header>
            <Modal.Title>Dynamic Model</Modal.Title>
            <Modal.Body>

                {field.map((val) => {
                    return (
                        <Input stepObject={props.stepObject} field_name={val.field_name} field_type={val.field_type} field={field} options={val.options} updateAdditionalDataHandler={updateAdditionalDataHandler} onchangeHandler={onchangeHandler}></Input>

                    )
                })}

                <button className="btn btn-primary btn-sm" onClick={onSubmitHandler}>Submit</button>

            </Modal.Body>

            <Modal.Footer>
                <button className="btn btn-danger btn-sm" onClick={handleClose}>
                    Close
                </button>
            </Modal.Footer>

        </Modal>
    </>)
}
export default ModelAdditionalDetails