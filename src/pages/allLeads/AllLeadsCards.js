import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { DragDropContext, Draggable, Droppable } from "react-beautiful-dnd";
// import { useLocation } from "react-router-dom";
import { LeadDispatchContext, LeadContext } from "../../context/LeadContext";
import { BASE_URL } from "../../util/constants";
import { instance } from "../../util/interceptor";
import ProjectModelDetails from "./cardOpenModel/ProjectModelDetails";
import ModelAdditionalDetails from "./ModelAdditionalDetails";
// import "./cardOpenModel/draging.css";
function AllLeadsCards() {
  const [columns, setColumns] = useState("");
  const [field, setField] = useState([]);
  const [data, setData] = useState([]);
  const [visible, setVisible] = useState(false);
  const [steps, setSteps] = useState([]);
  const [history, setHistory] = useState("");
  const [stepObject, setStepObject] = useState();
  // const [hasMoreLoad, setHasMoreLoad] = useState(true)
  const [limit, setLimit] = useState(10);
  const setLeadDetails = useContext(LeadDispatchContext);
  const leadDetails = useContext(LeadContext);
  console.log(leadDetails);
  let additionalFields = [];
  const [visibleAdditional, setVisibleAdditional] = useState(false);

  const [draggableResult, setDraggableResult] = useState();
  // const location=useLocation()
  // console.log(location.state)

  useEffect(() => {
    const userId = JSON.parse(localStorage.getItem("role")).id
    const token = JSON.parse(localStorage.getItem("userlogin")).access.token;
    axios
      .get(`${BASE_URL}/v1/lead/${userId}`)
      // .get(`${BASE_URL}/v1/lead`, {
      //   headers: {
      //     Authorization: `Bearer ${token}`,
      //   },
      // })

      .then((response) => {
        const totalData = formatData(response.data.result);
        // if(location.state!==null){
        //   totalData.Bid.items.unshift(location.state)
        // }

        console.log("Bis result", totalData);

        setColumns(totalData);
      })
      .catch((response) => {
        setColumns(response);
      });
  }, []);
  useEffect(() => {
    Object.entries(columns).map(([key, value], index) => {
      value.items.map((item, index) => {
        if (item.id === leadDetails.id) {
          item = { ...leadDetails };
          // console.log("id checking", item)
          value.items.splice(index, 1, item);
        }
      });
    });
  }, [leadDetails]);
  useEffect(() => {
    axios
      .get(`${BASE_URL}/v1/step`)
      .then((response) => {
        setSteps([...response.data.result]);
      })
      .catch((response) => {
        console.log(response);
      });
  }, []);
  function loadMoreData(e, columnName, page, limit, hasMoreLoad, pageCount) {
    const token = JSON.parse(localStorage.getItem("userlogin")).access.token;

    if (
      e.target.scrollHeight - e.target.clientHeight <
      e.target.scrollTop + 1
    ) {
      if (hasMoreLoad) {
        const userId = JSON.parse(localStorage.getItem("role")).id
        axios
          .get(`${BASE_URL}/v1/lead/${userId}/${columnName}/${page}/${limit}`)
          // .get(`${BASE_URL}/v1/lead/${columnName}/${page}/${limit}`, {
          //   headers: {
          //     Authorization: `Bearer ${token}`,
          //   },
          // })

          .then((response) => {
            if (response.data.page <= response.data.pageCount) {
              Object.entries(columns).map(([key, value], index) => {
                if (key === columnName) {
                  // value.hasMoreLoad = true
                  value.page = page + 1;
                  value.pageCount = response.data.pageCount;
                  if (value.page > value.pageCount) {
                    value.hasMoreLoad = false;
                  }
                  response.data.result.lead.map((val) => value.items.push(val));
                }
              });
              setColumns({ ...columns });
            }
          })
          .catch((response) => { });
      }
    }
  }

  const onDragEnd = async (result, columns, data, setData) => {
    console.log(result);
    if (!result.destination) return;
    setDraggableResult({ ...result });
    const { source, destination } = result;
    steps.filter((value) => {
      if (
        value.name === destination.droppableId &&
        value.fields.length !== 0 &&
        source.droppableId !== destination.droppableId
      ) {
        additionalFields.push(value);
        setStepObject(value);
      }
    });
    console.log(additionalFields);

    if (additionalFields.length !== 0) {
      console.log("open Model");
      openModelAdditional(additionalFields[0].fields);
    } else {
      console.log("step Change");
      stepOROrderChange(result, columns,  data, setData);
    }
  };
  async function stepOROrderChange(result, columns, data, setData) {
    const { source, destination } = result;
    console.log(result)

    if (source.droppableId !== destination.droppableId) {
      const sourceColumn = columns[source.droppableId];
      // console.log(sourceColumn);
      const destColumn = columns[destination.droppableId];
      // console.log(destColumn);
      const sourceItems = [...sourceColumn.items];
      // console.log(sourceItems);
      const destItems = [...destColumn.items];
      // console.log(destItems);
      const [removed] = sourceItems.splice(source.index, 1);
      removed.steps = destination.droppableId;
      // console.log(removed.id);
      destItems.splice(destination.index, 0, removed);
      // console.log(destItems);
      setColumns({
        ...columns,
        [source.droppableId]: {
          ...sourceColumn,
          items: sourceItems,
        },
        [destination.droppableId]: {
          ...destColumn,
          items: destItems,
        },
      });
      const url = `/v1/lead/movesteps/${removed.id}`;
      // const token = JSON.parse(localStorage.getItem("userlogin")).access.token;
      // const headers = {
      //   Authorization: `Bearer ${token}`,
      // };
      try {
        let response = await instance.patch(
          url,
          {
            steps: destination.droppableId,
          },
          {
            headers: {
              Authorization: `Bearer ${JSON.parse(localStorage.getItem("userlogin")).access.token
                }`,
            },
          }
        );

        console.log(response);
        setData((prevstate) => {
          return [
            ...prevstate,
            {
              source: source.droppableId,
              destination: destination.droppableId,
              data: response.data.result,
            },
          ];
        });
        return response;
      } catch (err) {
        console.log(err);
      }
    } else {
      const column = columns[source.droppableId];
      const copiedItems = [...column.items];
      const [removed] = copiedItems.splice(source.index, 1);
      copiedItems.splice(destination.index, 0, removed);
      setColumns({
        ...columns,
        [source.droppableId]: {
          ...column,
          items: copiedItems,
        },
      });
      // const url = `${BASE_URL}/v1/lead/movesteps/${removed.id}`
      const url = `/v1/lead/movesteps/${removed.id}`;

      // const token1 = JSON.parse(localStorage.getItem("userlogin")).access.token;
      // const headers1 = {
      //   Authorization: `Bearer ${token1}`,
      // };
      const response = await instance
        .patch(
          url,
          {
            steps: destination.droppableId,
          },
          {
            headers: {
              Authorization: `Bearer ${JSON.parse(localStorage.getItem("userlogin")).access.token
                }`,
            },
          }
        )
        .then((response) => {
          setData((prevstate) => {
            return [
              ...prevstate,
              {
                source: source.droppableId,
                destination: destination.droppableId,
                data: response.data.result,
              },
            ];
          });
          return data;
        })
        .catch((response) => {
          console.log(response);
        });
      return response;
    }
  }

  function openModelAdditional(field) {
    // console.log(field)
    setField([...field]);
    setVisibleAdditional((prevState) => !prevState);
    // onAdditionalDataSave()
  }

  function formatData(k) {
    let dataSet = {};
    Object.entries(k).map(([key, value], index) => {
      dataSet[key] = {
        name: key,
        items: value,
        page: 2,
        limit: limit,
        hasMoreLoad: true,
        pageCount: 1,
      };
    });
    return dataSet;
  }

  const onModelOpen = (item, name) => {
    setVisible((prevState) => !prevState);
    setLeadDetails(item);
  };
  function closeModel() {
    setLeadDetails(leadDetails);
    setVisible((prevState) => !prevState);
  }

  function historyData(item) {
    item.history.map((arr) => {
      setHistory({ ...arr });
      return history;
    });
  }
  async function onAdditionalDataSave(updatedAdditionalData) {
    const stepOROrderChangeData = await stepOROrderChange(
      draggableResult,
      columns,
      data,
      setData
    ).then((res) => {
      console.log("new return", res);
      return res.data.result;
    });

    console.log(data);
    console.log(stepOROrderChangeData);
    const updatedLeadData = updatedLead(updatedAdditionalData);
    console.log(updatedLeadData);
    async function updatedLead(updatedAdditionalData) {
      // const url = `${BASE_URL}/v1/lead/updatelead/${stepOROrderChangeData.id}`
      const url = `/v1/lead/updatelead/${stepOROrderChangeData.id}`;

      // const token = JSON.parse(localStorage.getItem("userlogin")).access.token;
      // const headers = {
      //   Authorization: `Bearer ${token}`,
      // };
      try {
        let response = await instance.patch(
          url,
          {
            additional_fields: updatedAdditionalData,
          },
          {
            headers: {
              Authorization: `Bearer ${JSON.parse(localStorage.getItem("userlogin")).access.token
                }`,
            },
          }
        );

        console.log(response);

        console.log("updatedLead", response);
        return response;
      } catch (err) {
        console.log(err);
      }
    }
  }
  const onSubmit = (updatedAdditionalData) => {
    onAdditionalDataSave(updatedAdditionalData);
  };
  function handleCloseAdditional() {
    setVisibleAdditional((prevState) => !prevState);
  }
  function selectDropdownHandler(result, columns) {
    onDragEnd(result, columns);
  }
  console.log(columns);

  // const [MousePosition, setMousePosition] = useState({
  //   left: 0,
  //   top: 0,
  // });

  // useEffect(() => {
  //   function handleMouseMove(ev) {
  //     setMousePosition({ left: ev.pageX, top: ev.pageY });
  //   }
  //   handleMouseMove()
  // });

  // function handleMouseMove(ev) {
  //   setMousePosition({ left: ev.pageX, top: ev.pageY });
  // }

  return (
    <>
      <ModelAdditionalDetails
        stepObject={stepObject}
        onSubmit={onSubmit}
        visible={visibleAdditional}
        handleCloseAdditional={handleCloseAdditional}
        field={field}
      ></ModelAdditionalDetails>
      <ProjectModelDetails
        selectDropdownHandler={selectDropdownHandler}
        visible={visible}
        closeModel={closeModel}
        columns={columns}
        steps={steps}
        history={history}
      ></ProjectModelDetails>
      <div
        className="content-wrapper"
      // onMouseMove={(ev) => handleMouseMove(ev)}
      // style={{ left: MousePosition.left, top: MousePosition.top, right: MousePosition.right }}

      >
        <div style={{ display: "flex", overflow: "scroll", height: "100%"  }}>
          <DragDropContext
            onDragEnd={(result) => onDragEnd(result, columns, data, setData)}
          >
            {Object.entries(columns).map(([columnId, column], index) => {
              return (
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "center",
                    height: "800px",
                  }}
                  key={columnId}
                >
                  <h2>{column.name}</h2>

                  <div
                    onScroll={(e) =>
                      loadMoreData(
                        e,
                        column.name,
                        column.page,
                        column.limit,
                        column.hasMoreLoad,
                        column.pageCount
                      )
                    }
                    style={{ margin: 8, height: "800px", overflowX: "scroll", }}
                  >
                    <Droppable droppableId={columnId} key={columnId}>
                      {(provided, snapshot) => {
                        return (
                          <div
                            {...provided.droppableProps}
                            ref={provided.innerRef}
                            style={{
                              background: snapshot.isDraggingOver
                                ? "lightblue"
                                : "lightgrey",
                              padding: 4,
                              width: 250,
                              minHeight: 500,
                              height: "800px",
                            }}
                          >
                            {column.items.map((item, index) => {
                              return (
                                <Draggable
                                  key={item.id}
                                  draggableId={item.id}
                                  index={index}

                                >
                                  {(provided, snapshot) => {
                                    return (
                                      <div
                                        onClick={() =>
                                          onModelOpen(item, column.name)
                                        }
                                        ref={provided.innerRef}
                                        {...provided.draggableProps}
                                        {...provided.dragHandleProps}
                                        style={{
                                          userSelect: "none",
                                          padding: 16,
                                          margin: "0 0 8px 0",
                                          minHeight: "50px",
                                          backgroundColor: snapshot.isDragging
                                            ? "#263B4A"
                                            : "#456C86",
                                          color: "white",
                                          ...provided.draggableProps.style,
                                        }}
                                      >
                                        {item.client_name}
                                      </div>
                                    );
                                  }}
                                </Draggable>
                              );
                            })}
                            {provided.placeholder}
                          </div>
                        );
                      }}
                    </Droppable>
                  </div>
                </div>
              );
            })}
          </DragDropContext>
        </div>
      </div>
    </>
  );
}

export default AllLeadsCards;
