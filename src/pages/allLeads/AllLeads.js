import PartialNavbar from "../../components/PartialNavbar"
import PartialSidebar from "../../components/PartialSidebar"
import { LeadProvider } from "../../context/LeadContext"
import AllLeadsCards from "./AllLeadsCards"

// import TopBanner from "../components/TopBanner"

const AllLeads = () => {
  return (
    <LeadProvider>
      <div className="container-scroller">
        {/* <TopBanner /> */}

        {/* <!-- partial:partials/_navbar.html --> */}
        <PartialNavbar />
        {/* <!-- partial --> */}
        <div className="container-fluid page-body-wrapper">
          {/* <!-- partial:partials/_sidebar.html --> */}
          <PartialSidebar />
          {/* <!-- partial --> */}

          <div className="main-panel">
            <AllLeadsCards />
          </div>
          {/* <!-- content-wrapper ends -->
          <!-- partial:partials/_footer.html --> */}

          {/* <!-- partial --> */}

          {/* <!-- main-panel ends --> */}
        </div>
        {/* <!-- page-body-wrapper ends --> */}
      </div>
      {/* <!-- container-scroller -->*/}
    </LeadProvider>
  )
}
export default AllLeads