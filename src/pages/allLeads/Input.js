import axios from "axios"
import { useState } from "react"
import { BASE_URL } from "../../util/constants"
import { instance } from "../../util/interceptor"

function Input(props) {
    const [field, setField] = useState(props.field)
    const [hasAdd, setHasAdd] = useState(false)
    const [hasSubmit, setHasSubmit] = useState(false)
    const [addOptionData, setAddOptionData] = useState("")
    const type = props.field_type
    const onchangeHandler = (e) => {
        const { value, name } = e.target

        //   setField(currentObj=>{
        //     currentObj.map(obj=>{
        //         if(obj.field_type==="Text"){
        //             // return{...obj,answer:value}
        //             obj.answer=value
        //             // console.log(obj)

        //         }
        //         // return obj

        //     })
        //   })
        //   console.log(field)
        // setField([...field, ...field.filter((item) => {
        //     if (item.field_name === name) {
        //         item.answer = value
        //     }
        // })])


        // setField(currentObj=>{
        //     currentObj.map(obj=>{
        //         if(obj.field_type==="CheckBox"){
        //             // return{...obj,answer:value}
        //             obj.answer=value
        //         }
        //         // return obj

        //     })
        //   })

        // setField([...field, ...field.filter((item) => {
        //     if (item.field_type === "CheckBox") {
        //         item.answer = value
        //     }
        // })])


        // setField(currentObj=>{
        //     currentObj.map(obj=>{
        //         if(obj.field_type==="Radio"){
        //             // return{...obj,answer:value}
        //             obj.answer=value
        //         }
        //         // return obj

        //     })
        //   })
        setField([...field, ...field.filter((item) => {
            if (item.field_name === name) {
                item.answer = value
            }
        })])



        // console.log(field)


    }
    // console.log("checking",inpval)
    // props.updateAdditionalDataHandler(field)
    console.log(field)
    const addHandler = (e) => {
        e.preventDefault()
        setHasAdd((prevState) => !prevState)
    }
    const onchangeAddHandler = (e) => {
        setAddOptionData(e.target.value)
    }
    const addSubmit = (e) => {
        e.preventDefault()
        setField([...field, ...field.filter((item) => {
            if (item.field_type === "Dropdown") {
                item.options.push(addOptionData)
            }
        })])

        // const url = `${BASE_URL}/v1/step/${props.stepObject.id}`
        // const url = `/v1/step/${props.stepObject.id}`

        // console.log(url)
        // const token1 = JSON.parse(localStorage.getItem("userlogin")).access.token;
        // const headers1 = {
        //     Authorization: `Bearer ${token1}`,
        // };
        // instance
        //     .patch(url, {
        //         name: props.stepObject.name,
        //         order: props.stepObject.order,
        //         fields: field


        //     },
        //         {
        //             headers: {
        //                 Authorization: `Bearer ${JSON.parse(localStorage.getItem("userlogin")).access.token}`,
        //             }
        //         })


        setHasAdd((prevState) => !prevState)
        setHasSubmit((prevState) => !prevState)

    }
    // console.log(props.stepObject)
    return (
        <>

            <form >
                {type === "Text" &&
                    <div className="form-group col-md-3">
                        <span className="label label-default">{props.field_name}</span>
                        <input name={props.field_name} className="form-control form-control-sm" onChange={(e)=>props.onchangeHandler(e)} type="text"></input>
                     

                    </div>
                }

                {type === "CheckBox" &&
                    <div className="form-group">
                        <span className="label label-default">{props.field_name}</span>
                        <div className="form-check form-check-flat form-check-primary">
                            <p className="form-check-label" style={{ color: "black" }}>
                                <ul style={{ display: "flex", gap: "30px" }}>
                                    {props.options.map(checkBoxValue => {
                                        return (
                                            <li className="form-check form-check-flat form-check-primary">
                                                <span
                                                    className="form-check-label"
                                                    style={{ color: "black" }}
                                                >
                                                    <input onChange={onchangeHandler} name={props.field_name} value={checkBoxValue} type="checkbox" />
                                                    {checkBoxValue}
                                                    <i className="input-helper"></i>
                                                </span>
                                            </li>
                                        )
                                    })}
                                </ul>
                            </p>
                        </div>
                    </div>

                }
                {type === "Radio" &&
                    <div className="form-group">
                        <span className="label label-default">{props.field_name}</span>
                        <div className="form-check">

                            {props.options.map(optionRadio => {
                                return (<>

                                    <input className="form-check-input" name={props.field_name} value={optionRadio} onChange={onchangeHandler} type="radio" />
                                    <label className="form-check-label" for="inlineRadio1">{optionRadio}</label>


                                </>
                                )


                            })}


                        </div>
                    </div>

                }
                <div className="form-group">
                    {type === "Dropdown" &&
                        <div>
                            <span className="label label-default">{props.field_name}</span>
                            <select onChange={onchangeHandler} name={props.field_name}>
                                {hasSubmit ? <option value={addOptionData}>{addOptionData}</option> : <option value="">Select</option>}

                                {props.options.map(optionDropdown => {
                                    return (
                                        <option value={optionDropdown}>{optionDropdown}</option>
                                    )
                                })}
                            </select>
                            {!hasAdd && <button onClick={addHandler}>Add</button>}
                            {hasAdd && <div><input onChange={onchangeAddHandler}  ></input>
                                <button onClick={addSubmit}>Submit Option</button>
                            </div>}
                        </div>

                    }
                </div>

            </form>

        </>
    )
}
export default Input