import { useState } from "react";
import PartialSidebar from "../../components/PartialSidebar"
import { BASE_URL } from "../../util/constants";
import { instance } from "../../util/interceptor";
import { useNavigate } from "react-router-dom";
import PartialNavbar from "../../components/PartialNavbar";


const AddUser = () => {
    const navigate = useNavigate();
    const [inpval, setInpval] = useState({
        email: "",
        name: "",
        password: "",
        role:""
    });
    // console.log(BASE_URL)
    const getdata = (e) => {
        const { value, name } = e.target;
        value.trim()

        // console.log(value)

        setInpval(() => {
            return {
                ...inpval,
                [name]: value,
                role:value
            };
        });
    };
    // const selectOptions=(e)=>{
    //     setInpval(() => {
    //         return {
    //             ...inpval,
    //             role:e.target.value,
    //         };
    //     });

    // }

    const addData = async (e) => {
        e.preventDefault();

        const { name, email, password } = inpval;

        if (!email && !password && !name) {
            alert("email and password and name is required");
        } else if (!email && password) {
            alert(" email is required");
        } else if (!email.includes("@")) {
            alert("email id must have @");
        } else if (email && !password) {
            alert("password is required");
        }
        else if (email && password && !name) {
            alert("name is required")

        } else {
            instance
                .post(`${BASE_URL}/v1/auth/register`, {
                    ...inpval,
                }
                    ,
                    {
                        headers: {
                            Authorization: `Bearer ${JSON.parse(localStorage.getItem("userlogin")).access.token}`,
                        }
                    }
                )
                .then((data) => {
                    console.log(data)
                    navigate('/manage_user')




                })
                .catch((error) => {
                    console.log(error)

                });
        }
    };
    console.log(inpval)
    return (
        <div className="container-scroller">
            <PartialNavbar/>
            <div className="container-fluid page-body-wrapper">

                <PartialSidebar />
                <div className="main-panel">
                    <div className="content-wrapper">
                        <div className="row">
                            <div className="col grid-margin stretch-card">
                                <div className="card">
                                    <div className="card-body">
                                        <form className="forms-sample row">
                                            <div className="form-group col-md-6">
                                                <label htmlFor="exampleInputName1"> Name</label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    name="name"
                                                    placeholder="Enter Name"
                                                    onChange={getdata}
                                                // // disabled={!hasEditDetails}
                                                // value={inpval.client_name}
                                                />

                                            </div>
                                            <div className="form-group col-md-6">
                                                <label htmlFor="exampleInputName1"> Email</label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    name="email"
                                                    placeholder="Enter Email"
                                                    onChange={getdata}
                                                // // disabled={!hasEditDetails}
                                                // value={inpval.client_name}
                                                />

                                            </div>
                                            <div className="form-group col-md-6">
                                                <label htmlFor="exampleInputName1"> Password</label>
                                                <input
                                                    type="password"
                                                    className="form-control"
                                                    name="password"
                                                    placeholder="Enter Password"
                                                    onChange={getdata}
                                                // // disabled={!hasEditDetails}
                                                // value={inpval.client_name}
                                                />

                                            </div>
                                            <div className="form-group col-md-2">
                                            <label htmlFor="exampleInputName1"> Select Role</label>

                                                <select name="role" className="form-select" onChange={getdata}>
                                                <option  value="">Select Role</option>
                                                    <option  value="user">User</option>
                                                    <option  value="admin">Admin</option>


                                                </select>

                                            </div>



                                        </form>
                                        <div className="form-group col-md-1">
                                            <button className="btn btn-success" onClick={addData}  >Submit</button>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    )
}
export default AddUser