import axios from 'axios';
import { useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';
import PartialSidebar from '../../components/PartialSidebar';
import { BASE_URL } from '../../util/constants';

function ManageUser() {
    const [data, setData] = useState([])
    useEffect(() => {

        axios
            .get(`${BASE_URL}/v1/users`, {
                headers: {
                    Authorization: `Bearer ${JSON.parse(localStorage.getItem("userlogin")).access.token}`,
                }
            })
            .then((response) => {
                console.log(response.data.result)
                setData([...response.data.result])
            })
            .catch((response) => {
                console.log(response)

            });

    }


        , [])
    return (
        // <div className="container-scroller">



        //     <div className="container-fluid page-body-wrapper">

        //         <PartialSidebar />


        //         <div className="main-panel">
        //             <NavLink to="/add_user">Add User</NavLink>
        //             <div className="row justify-content-center">

        //                 <Table size="sm">
        //                     <thead >
        //                         <tr >
        //                             <th>User Name</th>
        //                             <th>User Role</th>
        //                             <th>User Email</th>
        //                             <th>Update</th>


        //                         </tr>
        //                     </thead>
        // <tbody>
        //     {data.map((val)=>{
        //         return(
        //             <tr>
        //             <td>{val.name}</td>
        //             <td>{val.role}</td>
        //             <td>{val.email}</td>
        //             <td><NavLink to="/update_user" state={{ details: val }}>Edit</NavLink></td>
        //         </tr>
        //         )
        //     })}


        // </tbody>
        //                 </Table>
        //             </div>

        //         </div>

        //     </div>

        // </div>

        <div className="container-scroller">
            <div className="container-fluid page-body-wrapper">

                <PartialSidebar />
                <div className="main-panel">
                    <div className="content-wrapper">


                        <div className="page-header">
                            <h3 className="page-title"> <NavLink to="/add_user"  ><button className="btn btn-primary">Add User</button></NavLink></h3>

                        </div>
                        <div className="row">
                            <div className="col-lg-12 grid-margin stretch-card">
                                <div className="card">
                                    <div className="card-body">
                                        <h4 className="card-title">All Users Information</h4>
                                        <table className="table">
                                            <thead >
                                                <tr >
                                                    <th>User Name</th>
                                                    <th>User Role</th>
                                                    <th>User Email</th>
                                                    <th>Update</th>


                                                </tr>
                                            </thead>
                                            <tbody>
                                                {data.map((val) => {
                                                    return (
                                                        <tr>
                                                            <td>{val.name}</td>
                                                            <td>{val.role}</td>
                                                            <td>{val.email}</td>
                                                            <td><NavLink  to="/update_user" state={{ details: val }}><button className="btn btn-secondary">Edit</button></NavLink></td>
                                                        </tr>
                                                    )
                                                })}


                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>


    );
}

export default ManageUser;