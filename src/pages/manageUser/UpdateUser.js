// import axios from 'axios';
import { useState } from 'react';
import { useLocation } from 'react-router-dom'
import PartialSidebar from '../../components/PartialSidebar';
import { BASE_URL } from '../../util/constants';
import { instance } from '../../util/interceptor';
import { useNavigate } from "react-router-dom";
import PartialNavbar from '../../components/PartialNavbar';

const UpdateUser = () => {
    let location = useLocation();
    const [inpval, setInpval] = useState(location.state.details)
    const navigate = useNavigate();

    const getdata = (e) => {
        const { value, name } = e.target;
        // console.log(value)
        value.trim()

        setInpval(() => {
            return {
                ...inpval,
                [name]: value,
            };
        });
    };
    const addData = async (e) => {
        e.preventDefault();

        const { name, email, role } = inpval;

        if (!email && !name && !role) {
            alert("email and role and name is required");
        }
        else if (!email && name) {
            alert(" email is required");
        } else if (email && !name) {
            alert("name is required");
        }
        else if (email && name && !role) {
            alert("role is required")

        }
        else {
            instance
                .patch(`${BASE_URL}/v1/users/${inpval.id}`, {
                    name: inpval.name,
                    email: inpval.email,
                    role: inpval.role
                },
                    {
                        headers: {
                            Authorization: `Bearer ${JSON.parse(localStorage.getItem("userlogin")).access.token}`,
                        }
                    }
                )
                .then((data) => {
                    // console.log(data)
                    navigate('/manage_user');


                })
                .catch((error) => {
                    console.log(error)
                    alert(error.response.data.message);
                });
        }



    };

    return (
        <div className="container-scroller">
            <PartialNavbar />
            <div className="container-fluid page-body-wrapper">

                <PartialSidebar />
                <div className="main-panel">
                    <div className="content-wrapper">
                        <div className="row">
                            <div className="col grid-margin stretch-card">
                                <div className="card">
                                    <div className="card-body">
                                        <form className="forms-sample row">
                                            <div className="form-group col-md-6">
                                                <label htmlFor="exampleInputName1"> Name</label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    name="name"
                                                    // placeholder="Client Name"
                                                    onChange={getdata}
                                                    // // disabled={!hasEditDetails}
                                                    value={inpval.name}
                                                />

                                            </div>
                                            <div className="form-group col-md-6">
                                                <label htmlFor="exampleInputName1"> Email</label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    name="email"
                                                    // placeholder="Client Name"
                                                    onChange={getdata}
                                                    // // disabled={!hasEditDetails}
                                                    value={inpval.email}
                                                />

                                            </div>
                                            <div className="form-group col-md-6">
                                                <label htmlFor="exampleInputName1"> Role</label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    name="role"
                                                    // placeholder="Client Name"
                                                    onChange={getdata}
                                                    // // disabled={!hasEditDetails}
                                                    value={inpval.role}
                                                />

                                            </div>

                                        </form>
                                        <div className="form-group col-md-2">
                                            <button className="btn btn-success" onClick={addData}>Update</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



    )
}
export default UpdateUser